#ifndef __LCD_FUNCTION_H__
#define __LCD_FUNCTION_H__

void lcd_beginSDL();
void lcd_Destroy();
void lcd_CreatWindow_surface_texture();
void lcd_Screen_Refresh();
void lcd_ScreenSingleRowRefresh();
void lcd_ScreenSingleLineRefresh();
void lcd_ScreenRowLineRefresh();
void lcd_ScreenLineRowRefresh();
#endif
