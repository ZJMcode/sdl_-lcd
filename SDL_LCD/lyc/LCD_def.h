
#ifndef __LCD_DEF_H__
#define __LCD_DEF_H__

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <SDL2/SDL.h>//引用SDL库
#include "LCD_function.h"
#include "string.h"

#define LCD_Point_Pixel 2//lcd上的点的大小
#define LCD_Point_Pixel_Interval 0//lcd上的点之间像素大小
#define LCD_Point_W 128 //lcd上的宽度上的点数
#define LCD_Point_H 64 //lcd上的高度上的点数

#define USE_Slow_Time 0
#define Slow_Time_ms 10

#define Single_Row_Method 0
#define Single_Line_Method 1
#define Row_Line_Method 0
#define Line_Row_Method 0

#define Yin_Yang_Code 0 //1阳码 0阴码
#define Order_ReverseOrder 1//1顺向 0 逆向 从第一列开始向下 顺序取模就是取8个点作为一个字节，从低到高；逆序就是从高到低

#define WINDOW_W LCD_Point_W*(LCD_Point_Pixel+LCD_Point_Pixel_Interval)//LCD_Point_W-1每个像素宽度之间有1个像素的间隔
#define WINDOW_H LCD_Point_H*(LCD_Point_Pixel+LCD_Point_Pixel_Interval )//LCD_Point_H-1 每个像素高度之间有1个像素的间隔

#define LCD_Point_Background_Color        0, 0, 0, 180 //R,G,B,透明度
#define LCD_Point_Enable_Color                0, 255, 255, 255 //R,G,B,透明度
#define LCD_Point_Disenable_Color           0, 0, 100, 50 //R,G,B,透明度

#define Remainder_Add1(a) (a%8 > 0 ? (a/8 + 1): (a/8))

/**逆向：从第一列开始向下每取8个点作为一个字节，如果最后不足8个点就补满8位。
   取模顺序是从低到高，即第一个点作为最低位。如*-------取为00000001
**/
/**顺向：每取8个点作为一个字节，如果最后不足8个点就补满8位。
   取模顺序是从高到低，即第一个点作为最高位。如*-------取为10000000**/




//lcd显存
extern char lcd_points[LCD_Point_W][LCD_Point_H];

//窗口
extern SDL_Window* window ;
//渲染器
extern SDL_Renderer* rend ;
//布尔值 判断鼠标点击了界面右上角的叉号
extern bool quit;




#endif
