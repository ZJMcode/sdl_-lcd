/*************************************************************************/
/** Copyright.                                                          **/
/** FileName: LCD_function.c                                              **/
/** Author: ZJM                                                     **/
/** Description: LCD virtual function .          **/
/*************************************************************************/
#include "LCD_def.h"

//����
SDL_Window* window = NULL;
//��Ⱦ��
SDL_Renderer* rend = NULL;

char lcd_points[LCD_Point_W][LCD_Point_H] = {0};
/**����**/

void lcd_beginSDL()
{
    if ( SDL_WasInit(SDL_INIT_EVERYTHING) != 0 ) {
    printf("SDL is already running.\n");
    }
    if ( SDL_Init(SDL_INIT_EVERYTHING) == -1 ) {
    printf( "Unable to init SDL!");
    }
    printf( "SDL is running successfully.\n");
}
/**==����==**/
 //�˳�ʱ�����ڴ�
 void lcd_Destroy()
 {
    SDL_DestroyRenderer(rend);
    SDL_DestroyWindow(window);
    SDL_Quit();
 }
 //����window,��ʼ��LCD
 void lcd_CreatWindow_surface_texture()
 {
    window = SDL_CreateWindow("LCD",
                            SDL_WINDOWPOS_UNDEFINED,SDL_WINDOWPOS_UNDEFINED,
                            WINDOW_W,WINDOW_H,SDL_WINDOW_SHOWN);
    rend = SDL_CreateRenderer(window,-1,0);

    SDL_SetRenderDrawColor(rend, LCD_Point_Background_Color);
    SDL_RenderClear(rend);
    SDL_SetRenderDrawColor(rend, LCD_Point_Disenable_Color);

    SDL_RenderPresent(rend);
 }

//lcdˢ��
 void lcd_Screen_Refresh()
 {
    #if Single_Row_Method
    lcd_ScreenSingleRowRefresh();
    #endif
    #if Single_Line_Method
    lcd_ScreenSingleLineRefresh();
    #endif // Single_Line_Method
    #if Row_Line_Method
    lcd_ScreenRowLineRefresh();
    #endif // Row_Line_Method
    #if Line_Row_Method
    lcd_ScreenLineRowRefresh();
    #endif // Line_Row_Method
 }
 //����ʽ
 void lcd_ScreenLineRowRefresh()
 {
    int High_BNum =Remainder_Add1(LCD_Point_H);
    int Width_BNum = Remainder_Add1(LCD_Point_W);
    printf("%d\n",Width_BNum);
    SDL_Rect rect = {  0, 0 ,LCD_Point_Pixel, LCD_Point_Pixel};
    for(int Line = 0;Line < Width_BNum;Line++)
    {
        for(int i = 0;i < LCD_Point_H;i++)
        {
            rect.y = i*LCD_Point_Pixel+i*LCD_Point_Pixel_Interval;
            {
                for(int k = 0;k < 8 ; k++)
                {
                    #if Order_ReverseOrder
                    rect.x = (Line*8+k)*(LCD_Point_Pixel + LCD_Point_Pixel_Interval);
                    if(lcd_points[Line*8+k][i] == Yin_Yang_Code)//������
                    {
                        SDL_SetRenderDrawColor(rend, LCD_Point_Disenable_Color);
                        SDL_RenderFillRect(rend, &rect);
                    }
                    else
                    {
                        SDL_SetRenderDrawColor(rend, LCD_Point_Enable_Color);
                        SDL_RenderFillRect(rend, &rect);
                    }
                    #else
                    rect.x = ((Line+1)*8-1-k)*(LCD_Point_Pixel + LCD_Point_Pixel_Interval) ;
                    if(lcd_points[Line*8+7-k][i] == Yin_Yang_Code)//������
                    {
                        SDL_SetRenderDrawColor(rend, LCD_Point_Disenable_Color);
                        SDL_RenderFillRect(rend, &rect);
                    }
                    else
                    {
                        SDL_SetRenderDrawColor(rend, LCD_Point_Enable_Color);
                        SDL_RenderFillRect(rend, &rect);
                    }
                    #endif // Order_ReverseOrder

                    #if USE_Slow_Time
                    printf("%d\n",Line*8+k);
                    SDL_RenderPresent(rend);
                    SDL_Delay(Slow_Time_ms);
                    #endif // USE_Slow_Time
                }
            }
        }
        SDL_RenderPresent(rend);
    }
 }

//����ʽ
 void lcd_ScreenRowLineRefresh()
 {
    int High_BNum =Remainder_Add1(LCD_Point_H);
    int Width_BNum = Remainder_Add1(LCD_Point_W);
    SDL_Rect rect = {  0, 0 ,LCD_Point_Pixel, LCD_Point_Pixel};
    for(int Row = 0;Row < High_BNum;Row++)
    {
        for(int i = 0;i < LCD_Point_W;i++)
        {
            rect.x = i*LCD_Point_Pixel+i*LCD_Point_Pixel_Interval;
                for(int k = 0;k < 8 ; k++)
                {
                    #if Order_ReverseOrder
                    rect.y = (Row*8+k)*(LCD_Point_Pixel + LCD_Point_Pixel_Interval);
                    if(lcd_points[i][Row*8+k] == Yin_Yang_Code)//������
                    {
                        SDL_SetRenderDrawColor(rend, LCD_Point_Disenable_Color);
                        SDL_RenderFillRect(rend, &rect);
                    }
                    else
                    {
                        SDL_SetRenderDrawColor(rend, LCD_Point_Enable_Color);
                        SDL_RenderFillRect(rend, &rect);
                    }
                    #else
                    rect.y = ((Row+1)*8-1-k)*(LCD_Point_Pixel + LCD_Point_Pixel_Interval) ;
                    if(lcd_points[i][Row*8+7-k] == Yin_Yang_Code)//������
                    {
                        SDL_SetRenderDrawColor(rend, LCD_Point_Disenable_Color);
                        SDL_RenderFillRect(rend, &rect);
                    }
                    else
                    {
                        SDL_SetRenderDrawColor(rend, LCD_Point_Enable_Color);
                        SDL_RenderFillRect(rend, &rect);
                    }
                    #endif // Order_ReverseOrder

                    #if USE_Slow_Time
                    printf("%d\n",k);
                    SDL_RenderPresent(rend);
                    SDL_Delay(Slow_Time_ms);
                    #endif // USE_Slow_Time
                }
        }
        SDL_RenderPresent(rend);
    }

 }
//����ˢ��
 void lcd_ScreenSingleLineRefresh()
 {
    //memset(lcd_points, 1, sizeof(lcd_points)) ;
    int Width_BNum = Remainder_Add1(LCD_Point_W);
    SDL_Rect rect = {  0, 0 ,LCD_Point_Pixel, LCD_Point_Pixel};
    for(int i = 0;i < LCD_Point_H;i++)
    {
        rect.y = i*LCD_Point_Pixel+i*LCD_Point_Pixel_Interval;
        for(int j = 0;j < Width_BNum;j++)
        {
            for(int k = 0;k < 8 ; k++)
            {
                #if Order_ReverseOrder
                rect.x = (j*8+k)*(LCD_Point_Pixel + LCD_Point_Pixel_Interval);
                if(lcd_points[j*8+k][i] == Yin_Yang_Code)//������
                {
                    SDL_SetRenderDrawColor(rend, LCD_Point_Disenable_Color);
                    SDL_RenderFillRect(rend, &rect);
                }
                else
                {
                    SDL_SetRenderDrawColor(rend, LCD_Point_Enable_Color);
                    SDL_RenderFillRect(rend, &rect);
                }
                #else
                rect.x = ((j+1)*8-1-k)*(LCD_Point_Pixel + LCD_Point_Pixel_Interval) ;
                if(lcd_points[j*8+7-k][i] == Yin_Yang_Code)//������
                {
                    SDL_SetRenderDrawColor(rend, LCD_Point_Disenable_Color);
                    SDL_RenderFillRect(rend, &rect);
                }
                else
                {
                    SDL_SetRenderDrawColor(rend, LCD_Point_Enable_Color);
                    SDL_RenderFillRect(rend, &rect);
                }
                #endif // Order_ReverseOrder

                #if USE_Slow_Time
                printf("%d\n",j*8+k);
                SDL_RenderPresent(rend);
                SDL_Delay(Slow_Time_ms);
                #endif // USE_Slow_Time
            }
        }
    }
    SDL_RenderPresent(rend);
 }
 //lcd����ˢ��
 void lcd_ScreenSingleRowRefresh()
 {
   // memset(lcd_points, 1, sizeof(lcd_points)) ;
    int High_BNum = Remainder_Add1(LCD_Point_H);
    SDL_Rect rect = {  0, 0 ,LCD_Point_Pixel, LCD_Point_Pixel};
    for(int i = 0;i < LCD_Point_W;i++)
    {
        rect.x = i*LCD_Point_Pixel+i*LCD_Point_Pixel_Interval;
        for(int j = 0;j < High_BNum;j++)
        {
            for(int k = 0;k < 8 ; k++)
            {
                #if Order_ReverseOrder
                rect.y = (j*8+k)*(LCD_Point_Pixel + LCD_Point_Pixel_Interval);
                if(lcd_points[i][j*8+k] == Yin_Yang_Code)//������
                {
                    SDL_SetRenderDrawColor(rend, LCD_Point_Disenable_Color);
                    SDL_RenderFillRect(rend, &rect);
                }
                else
                {
                    SDL_SetRenderDrawColor(rend, LCD_Point_Enable_Color);
                    SDL_RenderFillRect(rend, &rect);
                }
                #else
                rect.y = ((j+1)*8-1-k)*(LCD_Point_Pixel + LCD_Point_Pixel_Interval) ;
                if(lcd_points[i][j*8+7-k] == Yin_Yang_Code)//������
                {
                    SDL_SetRenderDrawColor(rend, LCD_Point_Disenable_Color);
                    SDL_RenderFillRect(rend, &rect);
                }
                else
                {
                    SDL_SetRenderDrawColor(rend, LCD_Point_Enable_Color);
                    SDL_RenderFillRect(rend, &rect);
                }
                #endif // Order_ReverseOrder

                #if USE_Slow_Time
                printf("%d\n",j*8+k);
                SDL_RenderPresent(rend);
                SDL_Delay(Slow_Time_ms);
                #endif // USE_Slow_Time
            }
        }
    }
    SDL_RenderPresent(rend);
 }

