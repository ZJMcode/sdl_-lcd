#ifndef __SGUI_PORRT_H_
#define __SGUI_PORRT_H_

#include "SGUI_Basic.h"
#include "SGUI_Common.h"
#include "SGUI_Curve.h"
#include "SGUI_Text.h"
#include "SGUI_ItemsBase.h"
#include "SGUI_ScrollBar.h"
#include "SGUI_List.h"
#include "SGUI_Menu.h"
#include "SGUI_Notice.h"
#include "SGUI_PolarCoordinates.h"
#include "SGUI_ProcessBar.h"
#include "SGUI_RealtimeGraph.h"
#include "SGUI_VariableBox.h"
#include "LCD_def.h"

#define   HAL_Delay   SDL_Delay

//struct SGUI_Core
//{
//	public:
//		SGUI_SCR_DEV Scream_Operate;//��Ļ�����ռ䣬�������ݺͺ��� ���㡢��ʼ��������Ҫ�Լ�ʵ��
//
//    SGUI_Basic Basic;//gui��������
//		SGUI_Common Common;//�ַ�����ת��
//		SGUI_Curve Curve;//������
//		SGUI_Text Text;//�ı���ʾ
//		SGUI_ItemsBase ItemsBase;//��Ŀ����
//		SGUI_ScrollBar ScrollBar;//������
//		SGUI_List List;//�б�
//		SGUI_Menu Menu;//�˵����
//		SGUI_Notice Notice;//֪ͨ��
//		SGUI_PolarCoordinates PolarCoordinates;//������
//		SGUI_ProcessBar ProcessBar;//������
//		SGUI_RealtimeGraph RealtimeGraph;//ʵʱͼ��
//		SGUI_VariableBox VariableBox;//������
//
//		void Init();
//		void Test();
//	private:
//
//};
//extern SGUI_Core
extern SGUI_SCR_DEV Scream_Operate;

 int OLED_Init();
void OLED_Clear( void );
 void OLED_Set_Pixel(int x, int y,unsigned int color);//���Դ������õ�
 int OLED_Get_Pixel(int x, int y);//���Դ������õ�
 void GUI_Fill(int sx,int sy,int Width,int Height,unsigned int color);
void OLED_Display(void);
void SGUI_Init();
void SGUI_Test();

#endif
