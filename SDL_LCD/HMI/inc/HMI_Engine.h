#ifndef __INCLUDE_UHMI_ENGINE_H__
#define __INCLUDE_UHMI_ENGINE_H__
//=======================================================================//
//= Include files.                                                      =//
//=======================================================================//
#include "SGUI_Typedef.h"

//=======================================================================//
//= Macro definition.                                                   =//
//=======================================================================//
// GoBack history size
#define     HMI_SCREEN_HISTORY_MAX                          (20)
#define     HMI_EVENT_KEY_VALUE_LENGTH_MAX                  (4)
// Parameter post label.
#define     HMI_SCREEN_ID_ANY                               (-1)    // This label means parameter will posted to every screen.

#define     HMI_PROCESS_SUCCESSFUL(RESULT)                  (RESULT >= 0)
#define     HMI_PROCESS_FAILED(RESULT)                      (!(HMI_PROCESS_SUCCESSFUL(RESULT)))

// Start screen definition
#define     HMI_SCREEN_START                                (0)

#define     HMI_EVENT_TYPE_DECLARE(NAME, DATA)              typedef struct                                              \
                                                            {                                                           \
                                                                HMI_EVENT_BASE Head;                                    \
                                                                DATA Data;                                              \
                                                            }NAME;

#define     HMI_EVENT_DATA_MEMSET(EVENT)                    SGUI_SystemIF_MemorySet(&EVENT, 0x00, sizeof(EVENT))

#define     HMI_EVENT_INIT(EVENT)                           {                                                           \
                                                                HMI_EVENT_DATA_MEMSET(EVENT);                           \
                                                                EVENT.Head.iSize = sizeof(EVENT);                       \
                                                                EVENT.Head.iType = EVENT_TYPE_ANY;                      \
                                                            }

#define     HMI_EVENT_SIZE_CHK(EVENT, TYPE)                 (((EVENT).Head.iSize == sizeof(TYPE))?SGUI_TRUE:SGUI_FALSE)

//=======================================================================//
//= Data type definition.                                               =//
//=======================================================================//
#ifdef __cplusplus
extern "C"{
#endif
// HMI process result
typedef enum
{
    // Abnormal.
    HMI_RET_ERROR =                 -3,
    HMI_RET_INVALID_DATA =          -2,
    HMI_RET_ERROR_STATE =           -1,
    // Normal.
    HMI_RET_ABNORMAL =              0,
    HMI_RET_NORMAL =                1,
}HMI_ENGINE_RESULT;

typedef enum
{
    HMI_ENGINE_SCR_SWITCH =         0,// Switch screen and record to history.
    HMI_ENGINE_SCR_POPUP,           // Show up screen only.
}HMI_SCREEN_DISP_TYPE;

typedef struct
{
    SGUI_INT            iType;//事件类型
    SGUI_INT            iID;//事件来源
    SGUI_INT            iSize;//事件数据的实际大小，通常用作校验
}HMI_EVENT_BASE;//事件头信息结构体

typedef struct
{
    HMI_EVENT_BASE      Head;
}HMI_GENERAL_EVENT;

// Screen action interface function pointer structure.
typedef struct
{
    // Initialize screen data and parameter.
    HMI_ENGINE_RESULT               (*Initialize)   (SGUI_SCR_DEV* Interface);//初始化画面，此函数在HMI引擎初始化时被调用，每一个画面的准备
    // Do some thing before current screen display.
    HMI_ENGINE_RESULT               (*Prepare)      (SGUI_SCR_DEV* Interface, const void* pstParameters);//准备屏幕,初次显示，通常用于画面迁移的数据和环境准备，例如使能、失能外设、保存、读取数据等
    // Repaint screen if needed.
    HMI_ENGINE_RESULT               (*Repaint)      (SGUI_SCR_DEV* Interface, const void* pstParameters);//更新显示，刷新当前画面在屏幕上显示
    // Process event.
		HMI_ENGINE_RESULT               (*ProcessEvent) (SGUI_SCR_DEV* Interface, const HMI_EVENT_BASE* pstEvent, SGUI_INT* piActionID);//事件处理，当由事件投送到HMI引擎时，当前活动画面的该函数会被调用
    // Post process.
    HMI_ENGINE_RESULT               (*PostProcess)  (SGUI_SCR_DEV* Interface, HMI_ENGINE_RESULT eProcResult, SGUI_INT iActionID);//后处理，在处理事件的过程后被调用，通常用来判断事件处理的结果进行画面迁移设定，使能、失能外设、保存、读取数据等
}HMI_SCREEN_ACTION;//画面对象模型
// Screen data structure.
typedef struct _T_HMI_SCREEN_OBJECT_
{
    SGUI_INT                        iScreenID;//画面的ID，同一个HMI引擎管理下唯一标识画面对象
    HMI_SCREEN_ACTION*              pstActions;//画面对象动作的函数指针集合，画面的处理动作
		SGUI_INT 											  i_EventType;//筛选处理事件类型
		SGUI_INT											  i_EventID;//筛选处理事件ID
    struct _T_HMI_SCREEN_OBJECT_*   pstPrevious;//前一显示画面的对象指针，NULL意味着当前画面是最顶级画面

}HMI_SCREEN_OBJECT;

typedef struct
{
    HMI_SCREEN_OBJECT**             ScreenObjPtr;//指针数组，保存HMI引擎管理的所有画面对象的指针
    SGUI_INT                        ScreenCount;//HMI管理画面的总数，ScreenObjPtr的长度
    HMI_SCREEN_OBJECT*              CurrentScreenObject;//指向当前处于活动状态下的画面，与ScreenObjPtr中的某个值相同
    SGUI_SCR_DEV*                   Interface;//一个设备的对象模型，指向一个有效的数据对象模型
}HMI_ENGINE_OBJECT;
#ifdef __cplusplus
}
#endif
//=======================================================================//
//= Public function declaration.                                        =//
//=======================================================================//

HMI_ENGINE_RESULT   HMI_ActiveEngine(HMI_ENGINE_OBJECT* pstHMIEngineObject, SGUI_INT iScreenID);//激活HMI对象，设定初始化画面
HMI_ENGINE_RESULT   HMI_StartEngine(const void* pstParameters);//启动HMI对象，初始化画面
HMI_ENGINE_RESULT   HMI_ProcessEvent(const HMI_EVENT_BASE* pstEvent);//向HMI引擎投送事件,传感器，数据的变化
HMI_ENGINE_RESULT   HMI_SwitchScreen(SGUI_INT iDestScreenID, const void* pstParameters);//迁移指定画面
HMI_ENGINE_RESULT   HMI_GoBack(const void* pstParameters);//返回到上一画面
HMI_ENGINE_RESULT   HMI_SetDeviceObject(SGUI_SCR_DEV* pstDeviceObj);//为HMI对象设定对象

#endif // __INCLUDE_HMI_ENGINE_H__
