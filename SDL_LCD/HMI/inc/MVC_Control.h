#ifndef _MVC_CONTROL_H_
#define _MVC_CONTROL_H_

#include "SGUI_Typedef.h"

//输出变量
enum Knife{
			Knife_UP = 0,
			Knife_CENTER,
			Knife_DOWN,
			Knife_State_Num
		};//闸刀三个状态


extern float f_temp ;
extern SGUI_INT16 i16_Speed ;
extern SGUI_BOOL b_Relay1;
extern SGUI_BOOL b_Relay2 ;
extern SGUI_UINT8 u8_Knife ;
extern SGUI_INT16 i16_Voltage ;
extern SGUI_INT16 i16_Current ;
extern SGUI_INT16 i16_Humidity;
extern SGUI_INT16 i16_Wind_Speed ;

//输入变量
typedef union
	{
		float fValue;
		SGUI_INT32	dwValue;
	}tu_convert32;

#define VARIABLE_INPUT_NUM 10
	
typedef struct
{
	tu_convert32 VariableInput;
	SGUI_CHAR InputType;
}VariableInput;

extern VariableInput st_VariableInputBuffer[VARIABLE_INPUT_NUM] ;

	
#endif 
