//=======================================================================//
//= Include files.													    =//
//=======================================================================//
#include "MVC_Control.h"

//输出变量
float f_temp = 0;
SGUI_INT16 i16_Speed = 0;
SGUI_BOOL b_Relay1 = false;
SGUI_BOOL b_Relay2 = false;
SGUI_UINT8 u8_Knife = 0;
SGUI_INT16 i16_Voltage = 0;
SGUI_INT16 i16_Current = 0;
SGUI_INT16 i16_Humidity = 0;
SGUI_INT16 i16_Wind_Speed = 0;

//输入变量

VariableInput st_VariableInputBuffer[VARIABLE_INPUT_NUM] = {0};

