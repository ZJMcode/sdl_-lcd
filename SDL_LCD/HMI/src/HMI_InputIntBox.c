//=======================================================================//
//= Include files.													    =//
//=======================================================================//
//#include "DemoProc.h"
#include "SGUI_Port.h"
#include "HMI_Core.h"
#include "Resource.h"
#include "SGUI_Notice.h"
#include "SGUI_VariableBox.h"
#include "SGUI_FontResource.h"
#include "SGUI_IconResource.h"
#include "MVC_Control.h"
#include "DemoResource_GB2312.h"
//=======================================================================//
//= User Macro definition.											    =//
//=======================================================================//
#define						TEXT_INPUT_INT_LENGTH				(10)

#define						INPUT_INT_BOX_WIDTH					(100)
#define						INPUT_INT_NUMBER_BOX_HEIGHT			(8)
#define						INPUT_INT_TEXT_BOX_HEIGHT			(12)
#define						INPUT_INT_BOX_POSX					(10)
#define						INPUT_INT_BOX_NUMBER_POSY			(19)
#define						INPUT_INT_BOX_TEXT_POSY				(38)
//=======================================================================//
//= Static function declaration.									    =//
//=======================================================================//
static HMI_ENGINE_RESULT    HMI_InputIntBox_Initialize(SGUI_SCR_DEV* pstDeviceIF);
static HMI_ENGINE_RESULT	HMI_InputIntBox_Prepare(SGUI_SCR_DEV* pstDeviceIF, const void* pstParameters);
static HMI_ENGINE_RESULT	HMI_InputIntBox_RefreshScreen(SGUI_SCR_DEV* pstDeviceIF, const void* pstParameters);
static HMI_ENGINE_RESULT    HMI_InputIntBox_ProcessEvent(SGUI_SCR_DEV* pstDeviceIF, const HMI_EVENT_BASE* pstEvent, SGUI_INT* piActionID);
static HMI_ENGINE_RESULT	HMI_InputIntBox_PostProcess(SGUI_SCR_DEV* pstDeviceIF, HMI_ENGINE_RESULT eProcResult, SGUI_INT iActionID);
static void				    HMI_InputIntBox_DrawFrame(SGUI_SCR_DEV* pstDeviceIF, SGUI_SZSTR szTitle);

//=======================================================================//
//= Static variable declaration.									    =//
//=======================================================================//
static SGUI_NUM_VARBOX_STRUCT	s_stIntNumberVariableBox ;
const	SGUI_NUM_VARBOX_PARAM	s_stIntNumberVariableBoxParam =
	{
		{INPUT_INT_BOX_POSX+2, INPUT_INT_BOX_NUMBER_POSY+2, INPUT_INT_BOX_WIDTH, INPUT_INT_NUMBER_BOX_HEIGHT},
		-500,
		500,
		&SGUI_DEFAULT_FONT_8,
		SGUI_CENTER
	};

static SGUI_CHAR				s_szIntTextVariableBuffer[TEXT_INPUT_INT_LENGTH+1] = {"0123456789"};

static SGUI_TEXT_VARBOX_STRUCT	s_stIntTextVariableBox;
const SGUI_TEXT_VARBOX_PARAM s_stIntTextVariableBoxParam = {
		{INPUT_INT_BOX_POSX+2, INPUT_INT_BOX_TEXT_POSY+2, INPUT_INT_BOX_WIDTH, INPUT_INT_TEXT_BOX_HEIGHT},
		&SGUI_DEFAULT_FONT_12,
		TEXT_INPUT_INT_LENGTH,
};

static SGUI_CHAR				s_szDefaultFrameTitle[] =	SCR4_VAR_BOX_TITLE;
static SGUI_SZSTR				s_szFrameTitle =			s_szDefaultFrameTitle;
static SGUI_INT					s_uiFocusedFlag;
static SGUI_CSZSTR			s_szHelpNoticeText =		SCR4_HELP_NOTICE;
static SGUI_UINT32 			u32_VariableNumber = 0;//输入变量
static SGUI_UINT8				u8_VariableNumberCnt =		0;//输入变量计数
static VariableInput*   st_ptrVariableInput ;//MVC输入数组指针

HMI_SCREEN_ACTION				s_stInputIntBoxActions = {
																HMI_InputIntBox_Initialize,
																HMI_InputIntBox_Prepare,
																HMI_InputIntBox_RefreshScreen,
																HMI_InputIntBox_ProcessEvent,
																HMI_InputIntBox_PostProcess,
															};

//=======================================================================//
//= Global variable declaration.									    =//
//=======================================================================//
HMI_SCREEN_OBJECT       		g_stHMI_InputIntBox =	{	HMI_SCREEN_ID_INPUT_INT_BOX,
																&s_stInputIntBoxActions
															};

//=======================================================================//
//= Function define.										            =//
//=======================================================================//
HMI_ENGINE_RESULT HMI_InputIntBox_Initialize(SGUI_SCR_DEV* pstDeviceIF)
{
	//Initialize Event
		g_stHMI_InputIntBox.i_EventID = EVENT_ID_KEY_PRESS;
		g_stHMI_InputIntBox.i_EventType = EVENT_TYPE_ACTION;

	SGUI_NumberVariableBox_Initialize(&s_stIntNumberVariableBox, &s_stIntNumberVariableBoxParam);//初始化NUM Param
	SGUI_TextVariableBox_Initialize(&s_stIntTextVariableBox,&s_stIntTextVariableBoxParam, s_szIntTextVariableBuffer);
	s_uiFocusedFlag = 0;
	return HMI_RET_NORMAL;
}

HMI_ENGINE_RESULT HMI_InputIntBox_Prepare(SGUI_SCR_DEV* pstDeviceIF, const void* pstParameters)
{
	/*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
//	SGUI_NOTICT_BOX           	stNoticeBox;

	/*----------------------------------*/
	/* Initialize						*/
	/*----------------------------------*/
//	stNoticeBox.pstIcon = &SGUI_RES_ICON_INFORMATION_16;
//	stNoticeBox.cszNoticeText = s_szHelpNoticeText;
	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	u32_VariableNumber = 0;
	s_uiFocusedFlag = 0;
	u8_VariableNumberCnt = 0;

	st_ptrVariableInput = (VariableInput*)pstParameters;
	// Draw frame
	s_szFrameTitle = s_szDefaultFrameTitle;
	HMI_InputIntBox_DrawFrame(pstDeviceIF, (SGUI_SZSTR)s_szFrameTitle);

	// Draw number box
		s_stIntNumberVariableBox.stData.iValue = u32_VariableNumber;
		SGUI_Basic_DrawRectangle(pstDeviceIF, INPUT_INT_BOX_POSX, INPUT_INT_BOX_NUMBER_POSY, INPUT_INT_BOX_WIDTH+4, INPUT_INT_NUMBER_BOX_HEIGHT+4, SGUI_COLOR_FRGCLR, SGUI_COLOR_BKGCLR);
    SGUI_NumberVariableBox_Repaint(pstDeviceIF, &s_stIntNumberVariableBox, (0 == s_uiFocusedFlag)?SGUI_DRAW_REVERSE:SGUI_DRAW_NORMAL);

    // Draw text box
		s_stIntTextVariableBox.stData.iFocusIndex = 0;
    SGUI_Basic_DrawRectangle(pstDeviceIF, INPUT_INT_BOX_POSX, INPUT_INT_BOX_TEXT_POSY, INPUT_INT_BOX_WIDTH+4, INPUT_INT_TEXT_BOX_HEIGHT+4, SGUI_COLOR_FRGCLR, SGUI_COLOR_BKGCLR);
   SGUI_TextVariableBox_Repaint(pstDeviceIF, &s_stIntTextVariableBox, '\0', (0 == s_uiFocusedFlag)?SGUI_DRAW_NORMAL:SGUI_DRAW_REVERSE);

	// Start RTC
	//RTCTimerEnable(true);
	return HMI_RET_NORMAL;
}

HMI_ENGINE_RESULT HMI_InputIntBox_RefreshScreen(SGUI_SCR_DEV* pstDeviceIF, const void* pstParameters)
{
    /*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	// Draw frame
    HMI_InputIntBox_DrawFrame(pstDeviceIF, (SGUI_SZSTR)s_szFrameTitle);
    // Draw number box
		SGUI_Basic_DrawRectangle(pstDeviceIF, INPUT_INT_BOX_POSX, INPUT_INT_BOX_NUMBER_POSY, INPUT_INT_BOX_WIDTH+4, INPUT_INT_NUMBER_BOX_HEIGHT+4, SGUI_COLOR_FRGCLR, SGUI_COLOR_BKGCLR);
    SGUI_NumberVariableBox_Repaint(pstDeviceIF, &s_stIntNumberVariableBox, (0 == s_uiFocusedFlag)?SGUI_DRAW_REVERSE:SGUI_DRAW_NORMAL);

    // Draw text box
    SGUI_Basic_DrawRectangle(pstDeviceIF, INPUT_INT_BOX_POSX, INPUT_INT_BOX_TEXT_POSY, INPUT_INT_BOX_WIDTH+4, INPUT_INT_TEXT_BOX_HEIGHT+4, SGUI_COLOR_FRGCLR, SGUI_COLOR_BKGCLR);
   SGUI_TextVariableBox_Repaint(pstDeviceIF, &s_stIntTextVariableBox, '\0', (0 == s_uiFocusedFlag)?SGUI_DRAW_NORMAL:SGUI_DRAW_REVERSE);
		//SGUI_TextVariableBox_Paint(pstDeviceIF, &s_stTextVariableBox, (0 == s_uiFocusedFlag)?SGUI_DRAW_NORMAL:SGUI_DRAW_REVERSE);

	return HMI_RET_NORMAL;
}

HMI_ENGINE_RESULT HMI_InputIntBox_ProcessEvent(SGUI_SCR_DEV* pstDeviceIF, const HMI_EVENT_BASE* pstEvent, SGUI_INT* piActionID)
{
	/*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
	HMI_ENGINE_RESULT           eProcessResult;
	SGUI_UINT16					uiKeyValue;
	KEY_PRESS_EVENT*			pstKeyEvent;
	SGUI_INT					iProcessAction;

	/*----------------------------------*/
	/* Initialize						*/
	/*----------------------------------*/
	eProcessResult =			HMI_RET_NORMAL;
	iProcessAction =			HMI_DEMO_PROC_NO_ACT;

	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
			if(EVENT_TYPE_ACTION == pstEvent->iType && EVENT_ID_KEY_PRESS == pstEvent->iID)
			{
				pstKeyEvent = (KEY_PRESS_EVENT*)pstEvent;
				uiKeyValue = KEY_CODE_VALUE(pstKeyEvent->Data.uiKeyValue);

			switch(uiKeyValue)
			{
				case KEY_VALUE_TAB:
				{
					s_uiFocusedFlag = ((s_uiFocusedFlag+1)%2);
					if(0 == s_uiFocusedFlag)
					{
						SGUI_NumberVariableBox_Repaint(pstDeviceIF, &s_stIntNumberVariableBox, SGUI_DRAW_REVERSE);
						SGUI_TextVariableBox_Repaint(pstDeviceIF, &s_stIntTextVariableBox, '\0',SGUI_DRAW_NORMAL);
					}
					else
					{
						SGUI_NumberVariableBox_Repaint(pstDeviceIF, &s_stIntNumberVariableBox, SGUI_DRAW_NORMAL);
						SGUI_TextVariableBox_Repaint(pstDeviceIF, &s_stIntTextVariableBox, '\0',SGUI_DRAW_REVERSE);
					}
//						SGUI_NumberVariableBox_Repaint(pstDeviceIF, &s_stIntNumberVariableBox, SGUI_DRAW_NORMAL);
//						SGUI_TextVariableBox_Repaint(pstDeviceIF, &s_stIntTextVariableBox,'\0', SGUI_DRAW_REVERSE);
				break;
				}

				case KEY_VALUE_ESC:
				{
					iProcessAction = HMI_DEMO_PROC_CANCEL;
					break;
				}
				case KEY_VALUE_DOWN:
				{
					if(1 == s_uiFocusedFlag)
					{
						if(s_stIntTextVariableBox.stData.iFocusIndex > 0)
						{
							s_stIntTextVariableBox.stData.iFocusIndex--;
							//SGUI_TextVariableBox_ChangeCharacter(pstDeviceIF, &s_stTextVariableBox, SGUI_DRAW_REVERSE, SGUI_TEXT_ASCII, SGUI_TXT_VARBOX_OPT_NONE);
							SGUI_TextVariableBox_Repaint(pstDeviceIF, &s_stIntTextVariableBox, '\0', SGUI_DRAW_REVERSE);
						}
					}
					break;
				}
				case KEY_VALUE_UP:
				{
					if(1 == s_uiFocusedFlag)
					{
						if(s_stIntTextVariableBox.stData.iFocusIndex < (s_stIntTextVariableBox.stParam.iTextLength-1))
						{
							s_stIntTextVariableBox.stData.iFocusIndex++;
							SGUI_TextVariableBox_Repaint(pstDeviceIF, &s_stIntTextVariableBox, '\0', SGUI_DRAW_REVERSE);
						}
					}
					break;
				}
				case KEY_VALUE_ENTER:
				{
					if(1 == s_uiFocusedFlag)
					{
						// Draw number box
						if(u8_VariableNumberCnt||s_stIntTextVariableBox.stData.iFocusIndex)//过滤掉刚开始一直输入0的情况
						{
							u8_VariableNumberCnt++;//把输入值限定在9位数，否则有数据溢出的风险
						}
						if(u8_VariableNumberCnt<10)
						{
							if(s_stIntTextVariableBox.stData.iFocusIndex<10)//判断是否选择的是小数点
							{
								u32_VariableNumber = u32_VariableNumber*10+s_stIntTextVariableBox.stData.iFocusIndex;
							}
						}
						else
						{
							u8_VariableNumberCnt = 10;
						}

						s_stIntNumberVariableBox.stData.iValue = u32_VariableNumber;
						SGUI_Basic_DrawRectangle(pstDeviceIF, INPUT_INT_BOX_POSX, INPUT_INT_BOX_NUMBER_POSY, INPUT_INT_BOX_WIDTH+4, INPUT_INT_NUMBER_BOX_HEIGHT+4, SGUI_COLOR_FRGCLR, SGUI_COLOR_BKGCLR);
						SGUI_NumberVariableBox_Repaint(pstDeviceIF, &s_stIntNumberVariableBox, SGUI_DRAW_NORMAL);
					}
					else
					{
						iProcessAction = HMI_DEMO_PROC_CONFIRM;
					}
					break;
				}
				default:
				{
					/* No process. */
					break;
				}
			}
        }

  if(NULL != piActionID)
	{
		*piActionID = iProcessAction;
	}

	return eProcessResult;
}

HMI_ENGINE_RESULT HMI_InputIntBox_PostProcess(SGUI_SCR_DEV* pstDeviceIF, HMI_ENGINE_RESULT eProcResult, SGUI_INT iActionID)
{
	if(HMI_PROCESS_SUCCESSFUL(eProcResult))
	{
		if(HMI_DEMO_PROC_CANCEL == iActionID)
		{
//			s_uiAutoConfirmTimer = 5;
			HMI_GoBack(NULL);
		}
		if(HMI_DEMO_PROC_CONFIRM == iActionID)
		{
			if(NULL != st_ptrVariableInput)
			{
				st_ptrVariableInput->VariableInput.dwValue = u32_VariableNumber;
			}
			HMI_GoBack(NULL);
		}

	}

	return HMI_RET_NORMAL;
}

void HMI_InputIntBox_DrawFrame(SGUI_SCR_DEV* pstDeviceIF, SGUI_SZSTR szTitle)
{
	/*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
	SGUI_RECT				stTextDisplayArea;
	SGUI_POINT				stInnerPos;

	/*----------------------------------*/
	/* Initialize						*/
	/*----------------------------------*/
	stTextDisplayArea.iX =	4;
	stTextDisplayArea.iY =	4;
	stTextDisplayArea.iHeight = 12;
	stInnerPos.iX =			0;
	stInnerPos.iY =			0;

	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	if(NULL != pstDeviceIF)
	{
		stTextDisplayArea.iWidth = pstDeviceIF->stSize.iWidth-8;
		SGUI_Basic_DrawRectangle(pstDeviceIF, 0, 0, SGUI_RECT_WIDTH(pstDeviceIF->stSize), SGUI_RECT_HEIGHT(pstDeviceIF->stSize), SGUI_COLOR_FRGCLR, SGUI_COLOR_BKGCLR);
		SGUI_Basic_DrawRectangle(pstDeviceIF, 2, 2, SGUI_RECT_WIDTH(pstDeviceIF->stSize)-4, SGUI_RECT_HEIGHT(pstDeviceIF->stSize)-4, SGUI_COLOR_FRGCLR, SGUI_COLOR_TRANS);
		SGUI_Basic_DrawLine(pstDeviceIF, 3, 17, 124, 17, SGUI_COLOR_FRGCLR);
		SGUI_Text_DrawText(pstDeviceIF, szTitle, &GB2312_FZXS12, &stTextDisplayArea, &stInnerPos, SGUI_DRAW_NORMAL);
	}
}
