//=======================================================================//
//= Include files.													    =//
//=======================================================================//
//#include "DemoProc.h"
#include "SGUI_Port.h"
#include "HMI_Core.h"
#include "SGUI_List.h"
#include "SGUI_FontResource.h"
#include "Resource.h"
//#include "Key.h"
#include "MVC_Control.h"
#include <stdio.h>
#include <string.h>
//#include "main.h"

//=======================================================================//
//= User Macro definition.											    =//
//=======================================================================//
//#define					NOTICE_TEXT_BUFFER_SIZE				(64)

//=======================================================================//
//= Static function declaration.									    =//
//=======================================================================//
static HMI_ENGINE_RESULT	HMI_VariableList_Initialize(SGUI_SCR_DEV* pstDeviceIF);
static HMI_ENGINE_RESULT	HMI_VariableList_Prepare(SGUI_SCR_DEV* pstDeviceIF, const void* pstParameters);
static HMI_ENGINE_RESULT	HMI_VariableList_RefreshScreen(SGUI_SCR_DEV* pstDeviceIF, const void* pstParameters);
static HMI_ENGINE_RESULT	HMI_VariableList_ProcessEvent(SGUI_SCR_DEV* pstDeviceIF, const HMI_EVENT_BASE* pstEvent, SGUI_INT* piActionID);
static HMI_ENGINE_RESULT	HMI_VariableList_PostProcess(SGUI_SCR_DEV* pstDeviceIF, HMI_ENGINE_RESULT eProcResult, SGUI_INT iActionID);

#define VARIABLE_NUMB 9
#define VARIABLE_NUMB_SIZE 40
//#define VARIABLE_SIZE 6
//#define VARIABLE_MEASURE_SIZE 4

SGUI_CHAR c_VariableNameBuffer[VARIABLE_NUMB][VARIABLE_NUMB_SIZE] = {0};//显示list buffer
//SGUI_CHAR c_VariableBuffer[VARIABLE_NUMB][VARIABLE_SIZE] = {0};//变量buffer
//SGUI_CHAR c_VariableMeasureBuffer[VARIABLE_NUMB][VARIABLE_MEASURE_SIZE] = {0};//变量单位buffer


//=======================================================================//
//= Static variable declaration.									    =//
//=======================================================================//
static SGUI_ITEMS_ITEM		s_VariableListItems[] =		{
															{NULL, c_VariableNameBuffer[0],NULL,NULL},
															{NULL, c_VariableNameBuffer[1],NULL,NULL},
															{NULL, c_VariableNameBuffer[2],NULL,NULL},
															{NULL, c_VariableNameBuffer[3],NULL,NULL},
															{NULL, c_VariableNameBuffer[4],NULL,NULL},
															{NULL, c_VariableNameBuffer[5],NULL,NULL},
															{NULL, c_VariableNameBuffer[6],NULL,NULL},
															{NULL, c_VariableNameBuffer[7],NULL,NULL},
															{NULL, c_VariableNameBuffer[8],NULL,NULL}
														};
static SGUI_LIST		s_stVariableListObject = 		{0x00};

//=======================================================================//
//= Global variable declaration.									    =//
//=======================================================================//
HMI_SCREEN_ACTION		s_stVariableListActions =			{	HMI_VariableList_Initialize,
															HMI_VariableList_Prepare,
															HMI_VariableList_RefreshScreen,
															HMI_VariableList_ProcessEvent,
															HMI_VariableList_PostProcess
														};
HMI_SCREEN_OBJECT       g_stHMIVariable_List =				{	HMI_SCREEN_ID_VARIABLE_LIST,
															&s_stVariableListActions
														};

//=======================================================================//
//= Function define.										            =//
//=======================================================================//
HMI_ENGINE_RESULT HMI_VariableList_Initialize(SGUI_SCR_DEV* pstDeviceIF)
{
	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
		//Initialize Event
		g_stHMIVariable_List.i_EventID = EVENT_ID_KEY_PRESS|EVENT_ID_TIMER;
		g_stHMIVariable_List.i_EventType = EVENT_TYPE_ACTION;
    // Initialize list data.
    SGUI_SystemIF_MemorySet(&s_stVariableListObject, 0x00, sizeof(SGUI_LIST));
    // Title and font size must set before initialize list object.
    s_stVariableListObject.stLayout.iX = 0;
    s_stVariableListObject.stLayout.iY = 0;
    s_stVariableListObject.stLayout.iWidth = LCD_Point_W;
    s_stVariableListObject.stLayout.iHeight = LCD_Point_H;
    s_stVariableListObject.szTitle = SCR8_TITLE;
     //Initialize list object.

		sprintf(c_VariableNameBuffer[0],"设备温度  %5.2f℃",f_temp);
		sprintf(c_VariableNameBuffer[1],"设备速度  %5d m/s",i16_Speed);

		sprintf(c_VariableNameBuffer[2],"继电器1    ");
		strcat(c_VariableNameBuffer[2],b_Relay1?"开":"关");

		sprintf(c_VariableNameBuffer[3],"继电器2    ");
		strcat(c_VariableNameBuffer[3],b_Relay2?"开":"关");

		sprintf(c_VariableNameBuffer[4],"闸刀      ");
		strcat(c_VariableNameBuffer[4],(u8_Knife < Knife_State_Num ?(u8_Knife==Knife_UP? "上":(u8_Knife == Knife_CENTER?"中":"下")):"err"));

		sprintf(c_VariableNameBuffer[5],"电压      %5dV",i16_Voltage);
		sprintf(c_VariableNameBuffer[6],"电流      %5dA",i16_Current);
		sprintf(c_VariableNameBuffer[7],"设备湿度  %5dRH",i16_Humidity);
		sprintf(c_VariableNameBuffer[8],"风速      %-5dm/s",i16_Wind_Speed);

		SGUI_List_Initialize(&s_stVariableListObject, &s_stVariableListObject.stLayout,&GB2312_FZXS12, s_stVariableListObject.szTitle,s_VariableListItems, sizeof(s_VariableListItems)/sizeof(SGUI_ITEMS_ITEM));
		return HMI_RET_NORMAL;
}

HMI_ENGINE_RESULT HMI_VariableList_Prepare (SGUI_SCR_DEV* pstDeviceIF, const void* pstParameters)
{
	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	s_stVariableListObject.stItems.stSelection.iIndex = 0;
	s_stVariableListObject.stItems.stSelection.pstItem = &s_VariableListItems[s_stVariableListObject.stItems.stSelection.iIndex];

	SGUI_List_Repaint(pstDeviceIF, &s_stVariableListObject);
	return HMI_RET_NORMAL;
}

HMI_ENGINE_RESULT HMI_VariableList_RefreshScreen(SGUI_SCR_DEV* pstDeviceIF, const void* pstParameters)
{
	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	SGUI_List_Repaint(pstDeviceIF, &s_stVariableListObject);
	return HMI_RET_NORMAL;
}

HMI_ENGINE_RESULT HMI_VariableList_ProcessEvent(SGUI_SCR_DEV* pstDeviceIF, const HMI_EVENT_BASE* pstEvent, SGUI_INT* piActionID)
{
	/*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
	HMI_ENGINE_RESULT           eProcessResult;
	SGUI_UINT16					uiKeyCode;
	SGUI_UINT16					uiKeyValue;
	KEY_PRESS_EVENT*			pstKeyEvent;
	DATA_EVENT*					pstDataEvent;
	SGUI_INT					iProcessAction;

	/*----------------------------------*/
	/* Initialize						*/
	/*----------------------------------*/
	eProcessResult =			HMI_RET_NORMAL;
	pstKeyEvent =				(KEY_PRESS_EVENT*)pstEvent;
	iProcessAction =			HMI_DEMO_PROC_NO_ACT;

	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	if(pstEvent->iType == EVENT_TYPE_ACTION)
	{
		// Check event is valid.
		if(SGUI_FALSE == HMI_EVENT_SIZE_CHK(*pstKeyEvent, KEY_PRESS_EVENT))
		{
			// Event data is invalid.
			eProcessResult = HMI_RET_INVALID_DATA;
		}
		else if(EVENT_ID_KEY_PRESS == pstEvent->iID)
		{
			uiKeyCode = pstKeyEvent->Data.uiKeyValue;
			uiKeyValue = KEY_CODE_VALUE(uiKeyCode);
			pstKeyEvent->Data.uiKeyValue = KEY_VALUE_NONE;
			switch(uiKeyValue)
			{
				case KEY_VALUE_ENTER:
				{

					iProcessAction = HMI_DEMO_PROC_CONFIRM;
					break;
				}
				case KEY_VALUE_ESC:
				{
					iProcessAction = HMI_DEMO_PROC_CANCEL;
					break;
				}
				case KEY_VALUE_UP:
				{
					if(s_stVariableListObject.stItems.stSelection.iIndex > 0)
					{
						s_stVariableListObject.stItems.stSelection.iIndex -= 1;
					}
					s_stVariableListObject.stItems.stSelection.pstItem = &s_VariableListItems[s_stVariableListObject.stItems.stSelection.iIndex];
					SGUI_List_Repaint(pstDeviceIF, &s_stVariableListObject);
					break;
				}
				case KEY_VALUE_DOWN:
				{
					if(s_stVariableListObject.stItems.stSelection.iIndex < s_stVariableListObject.stItems.iCount-1)
					{
						s_stVariableListObject.stItems.stSelection.iIndex += 1;
					}
					s_stVariableListObject.stItems.stSelection.pstItem = &s_VariableListItems[s_stVariableListObject.stItems.stSelection.iIndex];
					SGUI_List_Repaint(pstDeviceIF, &s_stVariableListObject);
					break;
				}
				default:
				{
					break;
				}
			}
		}
		else if(EVENT_ID_TIMER == pstEvent->iID)
		{
			pstDataEvent = (DATA_EVENT*)pstEvent;
			if(SGUI_FALSE == HMI_EVENT_SIZE_CHK(*pstDataEvent, DATA_EVENT))
			{
				// Event data is invalid.
				eProcessResult = HMI_RET_INVALID_DATA;
			}
			else
			{
				f_temp = SDL_GetTicks()/400%1000*0.01;
				i16_Speed = SDL_GetTicks()/200%100;
				b_Relay1 = SDL_GetTicks()/1000%2;
				b_Relay2 = SDL_GetTicks()/1000%2;
				u8_Knife = SDL_GetTicks()/2000%4;
				i16_Voltage = SDL_GetTicks()/300%100;
				i16_Current = SDL_GetTicks()/200%50;
				i16_Humidity = SDL_GetTicks()/300%500;
				i16_Wind_Speed = SDL_GetTicks()/300%20;

				sprintf(c_VariableNameBuffer[0],"设备温度  %5.2f℃",f_temp);
				sprintf(c_VariableNameBuffer[1],"设备速度  %5d m/s",i16_Speed);

				sprintf(c_VariableNameBuffer[2],"继电器1    ");
				strcat(c_VariableNameBuffer[2],b_Relay1?"开":"关");

				sprintf(c_VariableNameBuffer[3],"继电器2    ");
				strcat(c_VariableNameBuffer[3],b_Relay2?"开":"关");

				sprintf(c_VariableNameBuffer[4],"闸刀      ");
				strcat(c_VariableNameBuffer[4],(u8_Knife < Knife_State_Num ?(u8_Knife==Knife_UP? "上":(u8_Knife == Knife_CENTER?"中":"下")):"err"));

				sprintf(c_VariableNameBuffer[5],"电压      %5dV",i16_Voltage);
				sprintf(c_VariableNameBuffer[6],"电流      %5dA",i16_Current);
				sprintf(c_VariableNameBuffer[7],"设备湿度  %5dRH",i16_Humidity);
				sprintf(c_VariableNameBuffer[8],"风速      %-5dm/s",i16_Wind_Speed);

				HMI_VariableList_RefreshScreen(pstDeviceIF, NULL);
			}
		}
	}


	if(NULL != piActionID)
	{
		*piActionID = iProcessAction;
	}

	return eProcessResult;
}

HMI_ENGINE_RESULT HMI_VariableList_PostProcess(SGUI_SCR_DEV* pstDeviceIF, HMI_ENGINE_RESULT eProcResult, SGUI_INT iActionID)
{

	 if(HMI_DEMO_PROC_CANCEL == iActionID)
	{
		HMI_GoBack(NULL);
	}

	return HMI_RET_NORMAL;
}


