#include "Key.h"
#include <string.h>
#include <SDL2/SDL.h>//引用SDL库


unsigned int uiKeyValue = KEY_VALUE_NONE;

//布尔值 判断鼠标点击了界面右上角的叉号
bool quit = true;

SDL_Event Key_event;
 /*事件处理*/
void  Key_HandleEvent()
{
    /**==事件==**/
        while (SDL_PollEvent(&Key_event))
        {
            if (Key_event.type == SDL_QUIT)
            {
                quit = false;
            }

            if (Key_event.type == SDL_KEYDOWN)
            {
                switch (Key_event.key.keysym.sym)
                {
                case SDLK_ESCAPE:
                    {
                        quit = false;
                        break;
                    }
                case SDLK_LEFT:
                    {
                       uiKeyValue = KEY_VALUE_ESC;
                        break;

                    }
                case SDLK_RIGHT:
                    {
                        uiKeyValue = KEY_VALUE_ENTER;
                        break;
                    }
                case SDLK_UP:
                    {
                        uiKeyValue = KEY_VALUE_UP;
                        break;
                    }
                case SDLK_DOWN:
                    {
                        uiKeyValue = KEY_VALUE_DOWN;
                        break;
                    }
                case SDLK_TAB:
                    {
                        uiKeyValue = KEY_VALUE_TAB;
                        break;
                    }
                }
            }
        }
}

