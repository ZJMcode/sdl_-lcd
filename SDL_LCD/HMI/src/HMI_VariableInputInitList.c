//=======================================================================//
//= Include files.													    =//
//=======================================================================//
//#include "DemoProc.h"
#include "SGUI_Port.h"
#include "HMI_Core.h"
#include "SGUI_List.h"
#include "SGUI_FontResource.h"
#include "Resource.h"
//#include "Key.h"
#include "MVC_Control.h"
#include <stdio.h>
#include <string.h>
//#include "main.h"

//=======================================================================//
//= User Macro definition.											    =//
//=======================================================================//
//#define					NOTICE_TEXT_BUFFER_SIZE				(64)

//=======================================================================//
//= Static function declaration.									    =//
//=======================================================================//
static HMI_ENGINE_RESULT	HMI_VariableInputInitList_Initialize(SGUI_SCR_DEV* pstDeviceIF);
static HMI_ENGINE_RESULT	HMI_VariableInputInitList_Prepare(SGUI_SCR_DEV* pstDeviceIF, const void* pstParameters);
static HMI_ENGINE_RESULT	HMI_VariableInputInitList_RefreshScreen(SGUI_SCR_DEV* pstDeviceIF, const void* pstParameters);
static HMI_ENGINE_RESULT	HMI_VariableInputInitList_ProcessEvent(SGUI_SCR_DEV* pstDeviceIF, const HMI_EVENT_BASE* pstEvent, SGUI_INT* piActionID);
static HMI_ENGINE_RESULT	HMI_VariableInputInitList_PostProcess(SGUI_SCR_DEV* pstDeviceIF, HMI_ENGINE_RESULT eProcResult, SGUI_INT iActionID);

#define VARIABLE_INPUT_INIT_NUMB 9
#define VARIABLE_INPUT_INIT_NUMB_SIZE 40
//#define VARIABLE_INPUT_INIT_SIZE 6
//#define VARIABLE_INPUT_INIT_MEASURE_SIZE 4

SGUI_CHAR c_VariableInputInitNameBuffer[VARIABLE_INPUT_INIT_NUMB][VARIABLE_INPUT_INIT_NUMB_SIZE] = {0};//显示list buffer
//SGUI_CHAR c_VariableInputInitBuffer[VARIABLE_INPUT_INIT_NUMB][VARIABLE_INPUT_INIT_SIZE] = {0};//变量buffer
//SGUI_CHAR c_VariableInputInitMeasureBuffer[VARIABLE_INPUT_INIT_NUMB][VARIABLE_INPUT_INIT_MEASURE_SIZE] = {0};//变量单位buffer


//=======================================================================//
//= Static variable declaration.									    =//
//=======================================================================//
static SGUI_ITEMS_ITEM		s_VariableInputInitListItems[] =		{
															{NULL, c_VariableInputInitNameBuffer[0],NULL,NULL},
															{NULL, c_VariableInputInitNameBuffer[1],NULL,NULL},
															{NULL, c_VariableInputInitNameBuffer[2],NULL,NULL},
															{NULL, c_VariableInputInitNameBuffer[3],NULL,NULL},
															{NULL, c_VariableInputInitNameBuffer[4],NULL,NULL},
															{NULL, c_VariableInputInitNameBuffer[5],NULL,NULL},
															{NULL, c_VariableInputInitNameBuffer[6],NULL,NULL},
															{NULL, c_VariableInputInitNameBuffer[7],NULL,NULL},
															{NULL, c_VariableInputInitNameBuffer[8],NULL,NULL}
														};
static SGUI_LIST		s_stVariableInputInitListObject = 		{0x00};

//=======================================================================//
//= Global variable declaration.									    =//
//=======================================================================//
HMI_SCREEN_ACTION		s_stVariableInputInitListActions =			{	HMI_VariableInputInitList_Initialize,
															HMI_VariableInputInitList_Prepare,
															HMI_VariableInputInitList_RefreshScreen,
															HMI_VariableInputInitList_ProcessEvent,
															HMI_VariableInputInitList_PostProcess
														};
HMI_SCREEN_OBJECT       g_stHMIVariableInputInit_List =				{	HMI_SCREEN_ID_VARIABLE_INPUT_INIT_LIST,
															&s_stVariableInputInitListActions
														};

//=======================================================================//
//= Function define.										            =//
//=======================================================================//
HMI_ENGINE_RESULT HMI_VariableInputInitList_Initialize(SGUI_SCR_DEV* pstDeviceIF)
{
	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
		//Initialize Event
		g_stHMIVariableInputInit_List.i_EventID = EVENT_ID_KEY_PRESS;
		g_stHMIVariableInputInit_List.i_EventType = EVENT_TYPE_ACTION;
    // Initialize list data.
    SGUI_SystemIF_MemorySet(&s_stVariableInputInitListObject, 0x00, sizeof(SGUI_LIST));
    // Title and font size must set before initialize list object.
    s_stVariableInputInitListObject.stLayout.iX = 0;
    s_stVariableInputInitListObject.stLayout.iY = 0;
    s_stVariableInputInitListObject.stLayout.iWidth = LCD_Point_W;
    s_stVariableInputInitListObject.stLayout.iHeight = LCD_Point_H;
    s_stVariableInputInitListObject.szTitle = SCR9_TITLE;
     //Initialize list object.

		sprintf(c_VariableInputInitNameBuffer[0],"设备温度  %5.2f℃",st_VariableInputBuffer[0].VariableInput.fValue);
		sprintf(c_VariableInputInitNameBuffer[1],"设备速度  %5d m/s",st_VariableInputBuffer[1].VariableInput.dwValue);

		sprintf(c_VariableInputInitNameBuffer[2],"继电器1    ");
		strcat(c_VariableInputInitNameBuffer[2],st_VariableInputBuffer[2].VariableInput.dwValue?"开":"关");

		sprintf(c_VariableInputInitNameBuffer[3],"继电器2    ");
		strcat(c_VariableInputInitNameBuffer[3],st_VariableInputBuffer[3].VariableInput.dwValue?"开":"关");

		sprintf(c_VariableInputInitNameBuffer[4],"闸刀      ");
		strcat(c_VariableInputInitNameBuffer[4],(st_VariableInputBuffer[4].VariableInput.dwValue < Knife_State_Num ?(st_VariableInputBuffer[4].VariableInput.dwValue==Knife_UP? "上":(st_VariableInputBuffer[4].VariableInput.dwValue == Knife_CENTER?"中":"下")):"err"));

		sprintf(c_VariableInputInitNameBuffer[5],"电压      %5dV",st_VariableInputBuffer[5].VariableInput.dwValue);
		sprintf(c_VariableInputInitNameBuffer[6],"电流      %5dA",st_VariableInputBuffer[6].VariableInput.dwValue);
		sprintf(c_VariableInputInitNameBuffer[7],"设备湿度  %5dRH",st_VariableInputBuffer[7].VariableInput.dwValue);
		sprintf(c_VariableInputInitNameBuffer[8],"风速      %-5dm/s",st_VariableInputBuffer[8].VariableInput.dwValue);

		SGUI_List_Initialize(&s_stVariableInputInitListObject, &s_stVariableInputInitListObject.stLayout,&GB2312_FZXS12, s_stVariableInputInitListObject.szTitle,s_VariableInputInitListItems, sizeof(s_VariableInputInitListItems)/sizeof(SGUI_ITEMS_ITEM));
		return HMI_RET_NORMAL;
}

HMI_ENGINE_RESULT HMI_VariableInputInitList_Prepare (SGUI_SCR_DEV* pstDeviceIF, const void* pstParameters)
{
	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	sprintf(c_VariableInputInitNameBuffer[0],"设备温度  %5.2f℃",st_VariableInputBuffer[0].VariableInput.fValue);
	sprintf(c_VariableInputInitNameBuffer[1],"设备速度  %5d m/s",st_VariableInputBuffer[1].VariableInput.dwValue);

	sprintf(c_VariableInputInitNameBuffer[2],"继电器1    ");
	strcat(c_VariableInputInitNameBuffer[2],st_VariableInputBuffer[2].VariableInput.dwValue?"开":"关");

	sprintf(c_VariableInputInitNameBuffer[3],"继电器2    ");
	strcat(c_VariableInputInitNameBuffer[3],st_VariableInputBuffer[3].VariableInput.dwValue?"开":"关");

	sprintf(c_VariableInputInitNameBuffer[4],"闸刀      ");
	strcat(c_VariableInputInitNameBuffer[4],(st_VariableInputBuffer[4].VariableInput.dwValue < Knife_State_Num ?(st_VariableInputBuffer[4].VariableInput.dwValue==Knife_UP? "上":(st_VariableInputBuffer[4].VariableInput.dwValue == Knife_CENTER?"中":"下")):"err"));

	sprintf(c_VariableInputInitNameBuffer[5],"电压      %5dV",st_VariableInputBuffer[5].VariableInput.dwValue);
	sprintf(c_VariableInputInitNameBuffer[6],"电流      %5dA",st_VariableInputBuffer[6].VariableInput.dwValue);
	sprintf(c_VariableInputInitNameBuffer[7],"设备湿度  %5dRH",st_VariableInputBuffer[7].VariableInput.dwValue);
	sprintf(c_VariableInputInitNameBuffer[8],"风速      %-5dm/s",st_VariableInputBuffer[8].VariableInput.dwValue);

	SGUI_List_Repaint(pstDeviceIF, &s_stVariableInputInitListObject);
	return HMI_RET_NORMAL;
}

HMI_ENGINE_RESULT HMI_VariableInputInitList_RefreshScreen(SGUI_SCR_DEV* pstDeviceIF, const void* pstParameters)
{
	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	SGUI_List_Repaint(pstDeviceIF, &s_stVariableInputInitListObject);
	return HMI_RET_NORMAL;
}

HMI_ENGINE_RESULT HMI_VariableInputInitList_ProcessEvent(SGUI_SCR_DEV* pstDeviceIF, const HMI_EVENT_BASE* pstEvent, SGUI_INT* piActionID)
{
	/*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
	HMI_ENGINE_RESULT           eProcessResult;
	SGUI_UINT16					uiKeyCode;
	SGUI_UINT16					uiKeyValue;
	KEY_PRESS_EVENT*			pstKeyEvent;
	DATA_EVENT*					pstDataEvent;
	SGUI_INT					iProcessAction;

	/*----------------------------------*/
	/* Initialize						*/
	/*----------------------------------*/
	eProcessResult =			HMI_RET_NORMAL;
	pstKeyEvent =				(KEY_PRESS_EVENT*)pstEvent;
	iProcessAction =			HMI_DEMO_PROC_NO_ACT;

	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	if(pstEvent->iType == EVENT_TYPE_ACTION)
	{
		// Check event is valid.
		if(SGUI_FALSE == HMI_EVENT_SIZE_CHK(*pstKeyEvent, KEY_PRESS_EVENT))
		{
			// Event data is invalid.
			eProcessResult = HMI_RET_INVALID_DATA;
		}
		else if(EVENT_ID_KEY_PRESS == pstEvent->iID)
		{
			uiKeyCode = pstKeyEvent->Data.uiKeyValue;
			uiKeyValue = KEY_CODE_VALUE(uiKeyCode);
			pstKeyEvent->Data.uiKeyValue = KEY_VALUE_NONE;
			switch(uiKeyValue)
			{
				case KEY_VALUE_ENTER:
				{

					iProcessAction = HMI_DEMO_PROC_CONFIRM;
					break;
				}
				case KEY_VALUE_ESC:
				{
					iProcessAction = HMI_DEMO_PROC_CANCEL;
					break;
				}
				case KEY_VALUE_UP:
				{
					if(s_stVariableInputInitListObject.stItems.stSelection.iIndex > 0)
					{
						s_stVariableInputInitListObject.stItems.stSelection.iIndex -= 1;
					}
					s_stVariableInputInitListObject.stItems.stSelection.pstItem = &s_VariableInputInitListItems[s_stVariableInputInitListObject.stItems.stSelection.iIndex];
					SGUI_List_Repaint(pstDeviceIF, &s_stVariableInputInitListObject);
					break;
				}
				case KEY_VALUE_DOWN:
				{
					if(s_stVariableInputInitListObject.stItems.stSelection.iIndex < s_stVariableInputInitListObject.stItems.iCount-1)
					{
						s_stVariableInputInitListObject.stItems.stSelection.iIndex += 1;
					}
					s_stVariableInputInitListObject.stItems.stSelection.pstItem = &s_VariableInputInitListItems[s_stVariableInputInitListObject.stItems.stSelection.iIndex];
					SGUI_List_Repaint(pstDeviceIF, &s_stVariableInputInitListObject);
					break;
				}
				default:
				{
					break;
				}
			}
		}
	}


	if(NULL != piActionID)
	{
		*piActionID = iProcessAction;
	}

	return eProcessResult;
}

HMI_ENGINE_RESULT HMI_VariableInputInitList_PostProcess(SGUI_SCR_DEV* pstDeviceIF, HMI_ENGINE_RESULT eProcResult, SGUI_INT iActionID)
{

	if(HMI_DEMO_PROC_CANCEL == iActionID)
	{
		HMI_GoBack(NULL);
	}
	if(HMI_DEMO_PROC_CONFIRM == iActionID)
	{
		switch(s_stVariableInputInitListObject.stItems.stSelection.iIndex)
		{
			case 0:
			{
				HMI_SwitchScreen(HMI_SCREEN_ID_VARIABLE_INPUT_FLOAT, &st_VariableInputBuffer[s_stVariableInputInitListObject.stItems.stSelection.iIndex]);
				break;
			}
			default:
			{
				HMI_SwitchScreen(HMI_SCREEN_ID_INPUT_INT_BOX, &st_VariableInputBuffer[s_stVariableInputInitListObject.stItems.stSelection.iIndex]);
				break;
			}
		}
	}

	return HMI_RET_NORMAL;
}


