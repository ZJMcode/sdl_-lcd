#include "HMI_Core.h"
#include "SGUI_Port.h"
#include "Key.h"
#include "LCD_def.h"
#include "stdio.h"
//#include "main.h"


//oled的RAM 显存
//数组每个bit存储OLED每个像素点的颜色值(1-亮(蓝色),0-灭(黑色))
//每个数组元素表示1列8个像素点，一共128列
static unsigned char OLED_OperateBuffer[LCD_Point_W*LCD_Point_H/8/2] =
{
	0x00
};
//=======================================================================//
//= Static variable declaration.									    =//
//=======================================================================//
SGUI_SCR_DEV				g_stDeviceInterface;
HMI_SCREEN_OBJECT*			g_arrpstScreenObjs[] =
							{
								&g_stHMIDemo_List,
								&g_stHMIDemo_TextPaint,
								&g_stHMIDemo_VariableBox,
								&g_stHMIDemo_RealtimeGraph,
								&g_stHMIDemo_Menu,
								&g_stHMIDemo_Notice,
								&g_stHMIDemo_BasicPaint,
								&g_stHMIVariable_List,
								&g_stHMI_InputIntBox,
								&g_stHMIVariableInputInit_List,
								&g_stHMI_InputFloatBox

							};
HMI_ENGINE_OBJECT			g_stDemoEngine;


HMI_ENGINE_RESULT InitHMIEngineObj()
{
	/*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
	HMI_ENGINE_RESULT           eProcessResult;
	int							iIndex;

	/*----------------------------------*/
	/* Initialize						*/
	/*----------------------------------*/
	eProcessResult =			HMI_RET_NORMAL;
	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	/* Clear structure. */
	SGUI_SystemIF_MemorySet(&g_stDeviceInterface, 0x00, sizeof(SGUI_SCR_DEV));
	SGUI_SystemIF_MemorySet(&g_stDemoEngine, 0x00, sizeof(HMI_ENGINE_OBJECT));

	/* Initialize display size. */
	g_stDeviceInterface.stSize.iWidth = LCD_Point_W;
	g_stDeviceInterface.stSize.iHeight = LCD_Point_H;
	g_stDeviceInterface.stBuffer.pBuffer = OLED_OperateBuffer;
	g_stDeviceInterface.stBuffer.sSize = LCD_Point_W*LCD_Point_H/8/2;
	/* Initialize interface object. */
	g_stDeviceInterface.fnSetPixel = OLED_Set_Pixel;
	g_stDeviceInterface.fnGetPixel = OLED_Get_Pixel;
	g_stDeviceInterface.fnClear = OLED_Clear;
	g_stDeviceInterface.fnSyncBuffer = OLED_Display;
	g_stDeviceInterface.fnInitialize = OLED_Init;
	g_stDeviceInterface.fnFillRect = GUI_Fill;

	do
	{
		/* Prepare HMI engine object. */
		g_stDemoEngine.ScreenCount = sizeof(g_arrpstScreenObjs)/sizeof(*g_arrpstScreenObjs);
		g_stDemoEngine.ScreenObjPtr = g_arrpstScreenObjs;
		g_stDemoEngine.Interface = &g_stDeviceInterface;

		/* Initialize all screen object. */
		if(NULL != g_stDemoEngine.ScreenObjPtr)
		{
			for(iIndex=0; iIndex<g_stDemoEngine.ScreenCount; iIndex++)
			{
				if( (NULL != g_stDemoEngine.ScreenObjPtr[iIndex])
					&& (NULL != g_stDemoEngine.ScreenObjPtr[iIndex]->pstActions)
					&& (NULL != g_stDemoEngine.ScreenObjPtr[iIndex]->pstActions->Initialize)
					)
				{
					g_stDemoEngine.ScreenObjPtr[iIndex]->pstActions->Initialize(&g_stDeviceInterface);
					g_stDemoEngine.ScreenObjPtr[iIndex]->pstPrevious = NULL;
				}
			}
		}
		else
		{

		}
		/* Active engine object. */
		eProcessResult = HMI_ActiveEngine(&g_stDemoEngine, HMI_SCREEN_ID_DEMO_LIST);
		//eProcessResult = HMI_ActiveEngine(&g_stDemoEngine, HMI_SCREEN_ID_DEMO_ITEMS_BASE);
		if(HMI_PROCESS_FAILED(eProcessResult))
		{
			/* Active engine failed. */
			break;
		}
		/* Start engine process. */
		eProcessResult = HMI_StartEngine(NULL);
		if(HMI_PROCESS_FAILED(eProcessResult))
		{
			/* Start engine failed. */
			break;
		}
	}while(0);

	return eProcessResult;
}

void KeyPressEventProc(void)
{
	/*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
	KEY_PRESS_EVENT		stEvent;

	/*----------------------------------*/
	/* Initialize						*/
	/*----------------------------------*/
	HMI_EVENT_INIT(stEvent);

	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	stEvent.Head.iType = EVENT_TYPE_ACTION;
	stEvent.Head.iID = EVENT_ID_KEY_PRESS;

	stEvent.Data.uiKeyValue = uiKeyValue;
	uiKeyValue = KEY_VALUE_NONE;

	// Post key press event.
	printf("Process key code 0x%04X.\r\n", stEvent.Data.uiKeyValue);
	HMI_ProcessEvent((HMI_EVENT_BASE*)(&stEvent));
}

void RealTimeEventProc(void)
{
	/*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
	DATA_EVENT		stEvent;

	/*----------------------------------*/
	/* Initialize						*/
	/*----------------------------------*/
	HMI_EVENT_INIT(stEvent);

	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	stEvent.Head.iType = EVENT_TYPE_ACTION;
	stEvent.Head.iID = EVENT_ID_TIMER;

	stEvent.Data.iValue = SDL_GetTicks()/10%100;

	// Post key press event.
	//printf("Process key code 0x%04X.\r\n", stEvent.Data.iValue);
	HMI_ProcessEvent((HMI_EVENT_BASE*)(&stEvent));
}


void HMI_Work()
{
    static int RealTime_cnt ;
	if(KEY_VALUE_NONE != uiKeyValue)
	{
		KeyPressEventProc();
	}
	if(SDL_GetTicks()- RealTime_cnt > 50)
	{
		RealTimeEventProc();
		RealTime_cnt = SDL_GetTicks();
	}
}
