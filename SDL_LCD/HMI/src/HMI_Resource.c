
/*************************************************************************/
/** Copyright.															**/
/** FileName: Resource.c												**/
/** Author: Polarix														**/
/** Version: 1.0.0.0													**/
/** Description: GB2312 font resource data, used FZXS-12 font.			**/
/*************************************************************************/
//=======================================================================//
//= Include files.													    =//
//=======================================================================//
#include "Resource.h"

//=======================================================================//
//= Static function declaration.									    =//
//=======================================================================//
static SGUI_INT			GetCharIndex_GB2312(SGUI_UINT32 uiCode);
static SGUI_CSZSTR		StepNext_GB2312(SGUI_CSZSTR cszSrc, SGUI_UINT32* puiCode);
static SGUI_SIZE		GB2312_GetFontData(SGUI_SIZE sStartAddr, SGUI_BYTE* pDataBuffer, SGUI_SIZE sReadSize);
static SGUI_BOOL		GB2312_IsFullWidth(SGUI_UINT32 uiCode);

//=======================================================================//
//= Global variable declaration.									    =//
//=======================================================================//
const SGUI_BYTE GB2312_H12[] = {
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// space	Index 0
0x00, 0x00, 0xFC, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00,	// !
0x00, 0x00, 0x0E, 0x00, 0x0E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// "
0x90, 0xD0, 0xBC, 0xD0, 0xBC, 0x90, 0x00, 0x03, 0x00, 0x03, 0x00, 0x00,	// #
0x18, 0x24, 0xFE, 0x44, 0x88, 0x00, 0x01, 0x02, 0x07, 0x02, 0x01, 0x00,	// $
0x18, 0x24, 0xD8, 0xB0, 0x4C, 0x80, 0x00, 0x03, 0x00, 0x01, 0x02, 0x01,	// %
0xC0, 0x38, 0xE4, 0x38, 0x80, 0x00, 0x01, 0x02, 0x02, 0x01, 0x02, 0x02,	// &
0x00, 0x00, 0x0E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// '
0x00, 0x00, 0x00, 0xF0, 0x0C, 0x02, 0x00, 0x00, 0x00, 0x00, 0x03, 0x04,	// (
0x02, 0x0C, 0xF0, 0x00, 0x00, 0x00, 0x04, 0x03, 0x00, 0x00, 0x00, 0x00,	// )
0x90, 0x60, 0xF8, 0x60, 0x90, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00,	// *
0x20, 0x20, 0xFC, 0x20, 0x20, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00,	// +
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x08, 0x06, 0x00, 0x00, 0x00, 0x00,	// ,
0x20, 0x20, 0x20, 0x20, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// -
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00,	// .
0x00, 0x80, 0x60, 0x18, 0x06, 0x00, 0x06, 0x01, 0x00, 0x00, 0x00, 0x00,	// /
0xF8, 0x04, 0x04, 0x04, 0xF8, 0x00, 0x01, 0x02, 0x02, 0x02, 0x01, 0x00,	// 0
0x00, 0x08, 0xFC, 0x00, 0x00, 0x00, 0x00, 0x02, 0x03, 0x02, 0x00, 0x00,	// 1
0x18, 0x84, 0x44, 0x24, 0x18, 0x00, 0x03, 0x02, 0x02, 0x02, 0x02, 0x00,	// 2
0x08, 0x04, 0x24, 0x24, 0xD8, 0x00, 0x01, 0x02, 0x02, 0x02, 0x01, 0x00,	// 3
0x40, 0xB0, 0x88, 0xFC, 0x80, 0x00, 0x00, 0x00, 0x02, 0x03, 0x02, 0x00,	// 4
0x3C, 0x24, 0x24, 0x24, 0xC4, 0x00, 0x01, 0x02, 0x02, 0x02, 0x01, 0x00,	// 5
0xF8, 0x24, 0x24, 0x24, 0xC8, 0x00, 0x01, 0x02, 0x02, 0x02, 0x01, 0x00,	// 6
0x0C, 0x04, 0xE4, 0x14, 0x0C, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00,	// 7
0xD8, 0x24, 0x24, 0x24, 0xD8, 0x00, 0x01, 0x02, 0x02, 0x02, 0x01, 0x00,	// 8
0x38, 0x44, 0x44, 0x44, 0xF8, 0x00, 0x01, 0x02, 0x02, 0x02, 0x01, 0x00,	// 9
0x00, 0x00, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00,	// :
0x00, 0x00, 0x30, 0x00, 0x00, 0x00, 0x00, 0x04, 0x03, 0x00, 0x00, 0x00,	// ;
0x00, 0x20, 0x50, 0x88, 0x04, 0x02, 0x00, 0x00, 0x00, 0x00, 0x01, 0x02,	// <
0x90, 0x90, 0x90, 0x90, 0x90, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// =
0x00, 0x02, 0x04, 0x88, 0x50, 0x20, 0x00, 0x02, 0x01, 0x00, 0x00, 0x00,	// >
0x18, 0x04, 0xC4, 0x24, 0x18, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00,	// ?
0xF8, 0x04, 0xE4, 0x94, 0x78, 0x00, 0x01, 0x02, 0x02, 0x02, 0x02, 0x00,	// @
0x00, 0xF0, 0x9C, 0x9C, 0xF0, 0x00, 0x02, 0x03, 0x00, 0x00, 0x03, 0x02,	// A
0x04, 0xFC, 0x24, 0x24, 0xD8, 0x00, 0x02, 0x03, 0x02, 0x02, 0x01, 0x00,	// B
0xF8, 0x04, 0x04, 0x04, 0x08, 0x00, 0x01, 0x02, 0x02, 0x02, 0x01, 0x00,	// C
0x04, 0xFC, 0x04, 0x04, 0xF8, 0x00, 0x02, 0x03, 0x02, 0x02, 0x01, 0x00,	// D
0x04, 0xFC, 0x24, 0x74, 0x0C, 0x00, 0x02, 0x03, 0x02, 0x02, 0x03, 0x00,	// E
0x04, 0xFC, 0x24, 0x74, 0x0C, 0x00, 0x02, 0x03, 0x02, 0x00, 0x00, 0x00,	// F
0xF8, 0x04, 0x04, 0x44, 0xCC, 0x40, 0x01, 0x02, 0x02, 0x02, 0x01, 0x00,	// G
0x04, 0xFC, 0x20, 0x20, 0xFC, 0x04, 0x02, 0x03, 0x00, 0x00, 0x03, 0x02,	// H
0x04, 0x04, 0xFC, 0x04, 0x04, 0x00, 0x02, 0x02, 0x03, 0x02, 0x02, 0x00,	// I
0x00, 0x04, 0x04, 0xFC, 0x04, 0x04, 0x02, 0x04, 0x04, 0x03, 0x00, 0x00,	// J
0x04, 0xFC, 0x24, 0xD0, 0x0C, 0x04, 0x02, 0x03, 0x02, 0x00, 0x03, 0x02,	// K
0x04, 0xFC, 0x04, 0x00, 0x00, 0x00, 0x02, 0x03, 0x02, 0x02, 0x02, 0x03,	// L
0xFC, 0x3C, 0xC0, 0x3C, 0xFC, 0x00, 0x03, 0x00, 0x03, 0x00, 0x03, 0x00,	// M
0x04, 0xFC, 0x30, 0xC4, 0xFC, 0x04, 0x02, 0x03, 0x02, 0x00, 0x03, 0x02,	// N
0xF8, 0x04, 0x04, 0x04, 0xF8, 0x00, 0x01, 0x02, 0x02, 0x02, 0x01, 0x00,	// O
0x04, 0xFC, 0x24, 0x24, 0x18, 0x00, 0x02, 0x03, 0x02, 0x00, 0x00, 0x00,	// P
0xF8, 0x84, 0x84, 0x04, 0xF8, 0x00, 0x01, 0x02, 0x02, 0x07, 0x04, 0x00,	// Q
0x04, 0xFC, 0x24, 0x64, 0x98, 0x00, 0x02, 0x03, 0x02, 0x00, 0x03, 0x02,	// R
0x18, 0x24, 0x24, 0x44, 0x88, 0x00, 0x01, 0x02, 0x02, 0x02, 0x01, 0x00,	// S
0x0C, 0x04, 0xFC, 0x04, 0x0C, 0x00, 0x00, 0x02, 0x03, 0x02, 0x00, 0x00,	// T
0x04, 0xFC, 0x00, 0x00, 0xFC, 0x04, 0x00, 0x01, 0x02, 0x02, 0x01, 0x00,	// U
0x04, 0x7C, 0x80, 0x80, 0x7C, 0x04, 0x00, 0x00, 0x03, 0x03, 0x00, 0x00,	// V
0x1C, 0xE0, 0x3C, 0xE0, 0x1C, 0x00, 0x00, 0x03, 0x00, 0x03, 0x00, 0x00,	// W
0x04, 0x9C, 0x60, 0x9C, 0x04, 0x00, 0x02, 0x03, 0x00, 0x03, 0x02, 0x00,	// X
0x04, 0x1C, 0xE0, 0x1C, 0x04, 0x00, 0x00, 0x02, 0x03, 0x02, 0x00, 0x00,	// Y
0x0C, 0x84, 0x64, 0x1C, 0x04, 0x00, 0x02, 0x03, 0x02, 0x02, 0x03, 0x00,	// Z
0x00, 0x00, 0xFE, 0x02, 0x02, 0x00, 0x00, 0x00, 0x07, 0x04, 0x04, 0x00,	// [
0x00, 0x0E, 0x30, 0xC0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x02, 0x00,	// '\'
0x00, 0x02, 0x02, 0xFE, 0x00, 0x00, 0x00, 0x04, 0x04, 0x07, 0x00, 0x00,	// ]
0x00, 0x04, 0x02, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// ^
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08,	// _
0x00, 0x00, 0x02, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// `
0x00, 0x40, 0xA0, 0xA0, 0xC0, 0x00, 0x00, 0x01, 0x02, 0x02, 0x01, 0x02,	// a
0x04, 0xFC, 0x20, 0x20, 0xC0, 0x00, 0x00, 0x03, 0x02, 0x02, 0x01, 0x00,	// b
0x00, 0xC0, 0x20, 0x20, 0x60, 0x00, 0x00, 0x01, 0x02, 0x02, 0x02, 0x00,	// c
0x00, 0xC0, 0x20, 0x24, 0xFC, 0x00, 0x00, 0x01, 0x02, 0x02, 0x01, 0x02,	// d
0x00, 0xC0, 0xA0, 0xA0, 0xC0, 0x00, 0x00, 0x01, 0x02, 0x02, 0x02, 0x00,	// e
0x00, 0x20, 0xF8, 0x24, 0x24, 0x04, 0x00, 0x02, 0x03, 0x02, 0x02, 0x00,	// f
0x00, 0x40, 0xA0, 0xA0, 0x60, 0x20, 0x00, 0x07, 0x0A, 0x0A, 0x0A, 0x04,	// g
0x04, 0xFC, 0x20, 0x20, 0xC0, 0x00, 0x02, 0x03, 0x00, 0x00, 0x03, 0x02,	// h
0x00, 0x20, 0xE4, 0x00, 0x00, 0x00, 0x00, 0x02, 0x03, 0x02, 0x00, 0x00,	// i
0x00, 0x00, 0x20, 0xE4, 0x00, 0x00, 0x08, 0x08, 0x08, 0x07, 0x00, 0x00,	// j
0x04, 0xFC, 0x80, 0xA0, 0x60, 0x20, 0x02, 0x03, 0x02, 0x00, 0x03, 0x02,	// k
0x04, 0x04, 0xFC, 0x00, 0x00, 0x00, 0x02, 0x02, 0x03, 0x02, 0x02, 0x00,	// l
0xE0, 0x20, 0xE0, 0x20, 0xC0, 0x00, 0x03, 0x00, 0x03, 0x00, 0x03, 0x00,	// m
0x20, 0xE0, 0x20, 0x20, 0xC0, 0x00, 0x02, 0x03, 0x02, 0x00, 0x03, 0x02,	// n
0x00, 0xC0, 0x20, 0x20, 0xC0, 0x00, 0x00, 0x01, 0x02, 0x02, 0x01, 0x00,	// o
0x20, 0xE0, 0x20, 0x20, 0xC0, 0x00, 0x08, 0x0F, 0x0A, 0x02, 0x01, 0x00,	// p
0x00, 0xC0, 0x20, 0x20, 0xE0, 0x00, 0x00, 0x01, 0x02, 0x0A, 0x0F, 0x08,	// q
0x20, 0xE0, 0x40, 0x20, 0x20, 0x00, 0x02, 0x03, 0x02, 0x00, 0x00, 0x00,	// r
0x00, 0x40, 0xA0, 0xA0, 0x20, 0x00, 0x00, 0x02, 0x02, 0x02, 0x01, 0x00,	// s
0x00, 0x20, 0xF8, 0x20, 0x00, 0x00, 0x00, 0x00, 0x01, 0x02, 0x02, 0x00,	// t
0x20, 0xE0, 0x00, 0x20, 0xE0, 0x00, 0x00, 0x01, 0x02, 0x02, 0x01, 0x02,	// u
0x20, 0xE0, 0x00, 0x00, 0xE0, 0x20, 0x00, 0x00, 0x03, 0x03, 0x00, 0x00,	// v
0x60, 0x80, 0xE0, 0x80, 0x60, 0x00, 0x00, 0x03, 0x00, 0x03, 0x00, 0x00,	// w
0x20, 0x60, 0x80, 0x60, 0x20, 0x00, 0x02, 0x03, 0x00, 0x03, 0x02, 0x00,	// x
0x20, 0xE0, 0x20, 0x80, 0x60, 0x20, 0x08, 0x08, 0x07, 0x01, 0x00, 0x00,	// y
0x00, 0x20, 0xA0, 0x60, 0x20, 0x00, 0x00, 0x02, 0x03, 0x02, 0x02, 0x00,	// z
0x00, 0x00, 0x60, 0x9E, 0x02, 0x00, 0x00, 0x00, 0x00, 0x07, 0x04, 0x00,	// {
0x00, 0x00, 0x00, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0F, 0x00, 0x00,	// |
0x00, 0x02, 0x9E, 0x60, 0x00, 0x00, 0x00, 0x04, 0x07, 0x00, 0x00, 0x00,	// }
0x04, 0x02, 0x02, 0x04, 0x04, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// ~		Index 94

0x42,0x22,0x5E,0x92,0x12,0xF2,0x00,0xFC,0x00,0x00,0xFF,0x00,0x00,0x08,0x04,0x02,
0x01,0x00,0x00,0x01,0x08,0x08,0x0F,0x00,//列0
0x40,0x44,0x54,0x54,0xD4,0x7F,0xD4,0x54,0x54,0x44,0x40,0x00,0x04,0x04,0x02,0x0F,
0x08,0x04,0x01,0x02,0x04,0x0A,0x09,0x00,//表1
0x98,0xD4,0xB3,0x88,0x00,0xFE,0x92,0x92,0x92,0xFE,0x00,0x00,0x04,0x04,0x02,0x02,
0x08,0x0F,0x08,0x08,0x08,0x0F,0x08,0x00,//组2
0x10,0xFC,0x03,0x90,0x8E,0x88,0x88,0xFF,0x88,0x88,0x88,0x00,0x00,0x0F,0x00,0x00,
0x00,0x00,0x00,0x0F,0x00,0x00,0x00,0x00,//件3
0x82,0x82,0xFF,0xAA,0xAA,0xAA,0xAA,0xAA,0xFF,0x82,0x82,0x00,0x0A,0x09,0x0A,0x0A,
0x0A,0x0F,0x0A,0x0A,0x0A,0x09,0x0A,0x00,//基4
0x42,0xF2,0x2E,0x22,0xE2,0x00,0xBC,0x20,0xFF,0x20,0xBC,0x00,0x00,0x07,0x02,0x02,
0x07,0x00,0x07,0x04,0x07,0x04,0x0F,0x00,//础5
0x98,0xD4,0xB3,0x08,0x88,0x94,0x92,0x91,0x92,0x94,0x88,0x00,0x04,0x04,0x02,0x00,
0x04,0x06,0x05,0x04,0x04,0x06,0x0C,0x00,//绘6
0xFF,0x21,0x29,0x2D,0x57,0x55,0x95,0x2D,0x21,0x21,0xFF,0x00,0x0F,0x04,0x04,0x05,
0x05,0x06,0x06,0x04,0x04,0x04,0x0F,0x00,//图7
0x88,0x88,0xFF,0x48,0xA4,0xAC,0xB5,0xE6,0xB4,0xAC,0xA4,0x00,0x00,0x08,0x0F,0x00,
0x08,0x0A,0x0B,0x04,0x04,0x0B,0x08,0x00,//接8
0x00,0xFE,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0xFE,0x00,0x00,0x00,0x07,0x02,0x02,
0x02,0x02,0x02,0x02,0x02,0x07,0x00,0x00,//口9
0x04,0x04,0x1C,0x64,0x85,0x06,0x84,0x64,0x1C,0x04,0x04,0x00,0x08,0x08,0x04,0x04,
0x02,0x01,0x02,0x04,0x04,0x08,0x08,0x00,//文10
0x04,0x04,0x84,0x64,0x14,0xFF,0x14,0x64,0x84,0x04,0x04,0x00,0x02,0x01,0x02,0x02,
0x02,0x0F,0x02,0x02,0x02,0x01,0x02,0x00,//本11
0x34,0x2C,0xF7,0xA4,0xE8,0xA4,0xEA,0x09,0xCA,0x04,0xE8,0x00,0x01,0x01,0x0F,0x00,
0x0F,0x02,0x0F,0x00,0x03,0x08,0x0F,0x00,//输12
0x00,0x00,0x01,0x81,0x72,0x0C,0x70,0x80,0x00,0x00,0x00,0x00,0x08,0x04,0x02,0x01,
0x00,0x00,0x00,0x01,0x02,0x04,0x08,0x00,//入13
0x88,0x68,0xFF,0x48,0xFE,0x02,0x4A,0x4A,0xFA,0x4A,0x4A,0x00,0x00,0x00,0x0F,0x00,
0x0F,0x08,0x0A,0x0A,0x0B,0x0A,0x0A,0x00,//框14
0x86,0x82,0xA2,0xCA,0x92,0x83,0xFA,0x82,0x82,0x82,0x86,0x00,0x08,0x08,0x04,0x04,
0x02,0x01,0x00,0x02,0x02,0x04,0x08,0x00,//实15
0xFE,0x22,0x22,0xFE,0x00,0x08,0x48,0x88,0x08,0xFF,0x08,0x00,0x07,0x02,0x02,0x07,
0x00,0x00,0x00,0x09,0x08,0x0F,0x00,0x00,//时16
0x00,0xFC,0x44,0x44,0xFF,0x44,0x44,0xFF,0x44,0x44,0xFC,0x00,0x00,0x0F,0x04,0x04,
0x07,0x04,0x04,0x07,0x04,0x04,0x0F,0x00,//曲17
0x98,0xD4,0xB3,0x88,0x00,0x48,0x48,0xFF,0x24,0xA5,0x26,0x00,0x04,0x04,0x02,0x02,
0x08,0x08,0x04,0x03,0x05,0x08,0x0E,0x00,//线18
0x12,0x32,0x52,0x17,0x32,0xD2,0x12,0x4F,0x2A,0x0A,0x02,0x00,0x09,0x09,0x05,0x03,
0x01,0x0F,0x01,0x03,0x05,0x09,0x09,0x00,//菜19
0x00,0x7C,0x55,0x56,0x54,0xFC,0x54,0x56,0x55,0x7C,0x00,0x00,0x01,0x01,0x01,0x01,
0x01,0x0F,0x01,0x01,0x01,0x01,0x01,0x00,//单20
0x10,0x22,0x04,0x00,0xF2,0x54,0x50,0x5F,0x50,0x54,0xF2,0x00,0x04,0x02,0x01,0x00,
0x0F,0x01,0x01,0x01,0x01,0x09,0x0F,0x00,//消21
0x00,0xFE,0xAA,0xAA,0xAB,0xAA,0xAA,0xAA,0xAA,0xFE,0x00,0x00,0x08,0x06,0x00,0x06,
0x08,0x09,0x0A,0x08,0x0C,0x02,0x0C,0x00,//息22
0x10,0x12,0x12,0xFF,0x91,0x10,0xFF,0x10,0x90,0x52,0x14,0x00,0x02,0x02,0x09,0x0F,
0x00,0x04,0x02,0x03,0x04,0x08,0x0E,0x00,//我23
0x02,0x02,0x82,0x42,0x22,0xF2,0x0E,0x22,0x42,0x82,0x02,0x00,0x01,0x01,0x00,0x00,
0x00,0x0F,0x00,0x00,0x00,0x00,0x01,0x00,//不24
0x02,0xEA,0xAA,0xAA,0xAA,0xAF,0xAA,0xAA,0xAA,0xEA,0x02,0x00,0x02,0x0E,0x0B,0x0A,
0x0A,0x0A,0x0A,0x0A,0x0B,0x0E,0x02,0x00,//喜25
0x0A,0x32,0xC2,0x32,0x1E,0x08,0x07,0xE4,0x04,0x14,0x0C,0x00,0x04,0x03,0x00,0x03,
0x08,0x04,0x03,0x00,0x03,0x04,0x08,0x00,//欢26
0x10,0x11,0xF2,0x00,0x04,0x14,0xA5,0x46,0xA4,0x1C,0x04,0x00,0x08,0x04,0x03,0x04,
0x0A,0x09,0x08,0x08,0x08,0x09,0x0A,0x00,//这27
0x20,0x20,0x10,0x08,0x04,0xF3,0x04,0x08,0x10,0x20,0x20,0x00,0x00,0x00,0x00,0x00,
0x00,0x0F,0x00,0x00,0x00,0x00,0x00,0x00,//个28
0x00,0x7F,0x49,0x49,0x49,0xFF,0x49,0x49,0x49,0x7F,0x00,0x00,0x08,0x09,0x09,0x09,
0x09,0x0F,0x09,0x09,0x09,0x09,0x08,0x00,//里29
0x40,0x40,0x5F,0x55,0x55,0xD5,0x55,0x55,0x5F,0x40,0x40,0x00,0x08,0x04,0x03,0x04,
0x08,0x0F,0x09,0x09,0x09,0x09,0x08,0x00,//是30
0x10,0xFC,0x0B,0xBC,0xAB,0x6A,0xBA,0x2A,0x2E,0xB8,0x00,0x00,0x00,0x0F,0x00,0x0A,
0x0A,0x05,0x0A,0x0F,0x01,0x02,0x04,0x00,//像31
0x22,0x2A,0xAA,0xEA,0xAA,0xBF,0xAA,0x6A,0x2A,0x2A,0x22,0x00,0x00,0x0A,0x06,0x02,
0x0B,0x0E,0x02,0x02,0x07,0x0A,0x00,0x00,//素32
0xFC,0x46,0x45,0x44,0xFC,0x10,0x08,0x27,0xC4,0x04,0xFC,0x00,0x0F,0x04,0x04,0x04,
0x0F,0x00,0x00,0x00,0x08,0x08,0x07,0x00,//的33
0x90,0x90,0x48,0x54,0x23,0x92,0x4A,0x66,0x52,0xC0,0x40,0x00,0x08,0x08,0x08,0x09,
0x05,0x04,0x03,0x02,0x01,0x00,0x00,0x00,//多34
0x48,0x24,0xF2,0x09,0x10,0x12,0x12,0x12,0xF2,0x12,0x10,0x00,0x00,0x00,0x0F,0x00,
0x00,0x00,0x08,0x08,0x0F,0x00,0x00,0x00,//行35
0x10,0x22,0x04,0x00,0xE6,0xAA,0xAA,0xFB,0xAA,0xAA,0xE6,0x00,0x04,0x02,0x01,0x00,
0x0B,0x06,0x02,0x03,0x02,0x06,0x0B,0x00,//演36
0x10,0x10,0x92,0x12,0x12,0xF2,0x12,0x12,0x92,0x10,0x10,0x00,0x04,0x02,0x01,0x00,
0x08,0x0F,0x00,0x00,0x00,0x01,0x06,0x00,//示37
0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x00,0x00,0x00,0x00,0x00,//
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,//一0
0x20,0xA4,0x92,0x97,0x8A,0xEA,0x8A,0x96,0x92,0xA0,0x20,0x00,0x08,0x04,0x02,0x00,//
0x08,0x0F,0x00,0x00,0x02,0x04,0x08,0x00,//条1
0x00,0xF8,0x88,0x88,0x88,0xFF,0x88,0x88,0x88,0xF8,0x00,0x00,0x00,0x01,0x00,0x00,//
0x00,0x0F,0x00,0x00,0x00,0x01,0x00,0x00,//中2
0x88,0x88,0xFF,0x48,0x08,0x5F,0x55,0xD5,0x55,0x5F,0x40,0x00,0x00,0x08,0x0F,0x00,//
0x08,0x07,0x08,0x0F,0x09,0x09,0x08,0x00,//提3
0x10,0x21,0x02,0x28,0xE7,0x21,0x21,0x21,0xA7,0x68,0x08,0x00,0x04,0x02,0x09,0x08,//
0x04,0x05,0x02,0x05,0x04,0x08,0x08,0x00,//没4
0x44,0x24,0xF4,0x5C,0x57,0x54,0x54,0x54,0x54,0xF4,0x04,0x00,0x00,0x00,0x0F,0x01,//
0x01,0x01,0x01,0x01,0x09,0x0F,0x00,0x00,//有5
0x88,0x68,0xFF,0x48,0x10,0x92,0x12,0xF2,0x12,0x92,0x10,0x00,0x00,0x00,0x0F,0x00,//
0x02,0x01,0x08,0x0F,0x00,0x00,0x03,0x00,//标6
0x00,0x00,0xC0,0xC0,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x02,0x01,//
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,//，0
0x00,0x00,0xC0,0x20,0x20,0xC0,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x01,//
0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,//。1
0x02,0x05,0x02,0xF8,0x04,0x02,0x02,0x02,0x04,0x0E,0x80,0x00,0x00,0x00,0x00,0x00,//
0x01,0x02,0x02,0x02,0x02,0x01,0x00,0x00,//℃2
0x10,0x11,0xF2,0x00,0x50,0xCF,0x41,0x41,0x4F,0xD0,0x10,0x00,0x00,0x00,0x07,0x02,//
0x08,0x08,0x05,0x02,0x05,0x08,0x08,0x00,//设3
0x20,0x24,0xD2,0x57,0x4A,0xCA,0x4A,0x56,0xD2,0x20,0x20,0x00,0x00,0x00,0x0F,0x05,//
0x05,0x07,0x05,0x05,0x0F,0x00,0x00,0x00,//备4
0x22,0x44,0x00,0xC0,0x5F,0xD5,0x55,0xD5,0x5F,0xC0,0x00,0x00,0x04,0x02,0x09,0x0F,//
0x08,0x0F,0x08,0x0F,0x08,0x0F,0x08,0x00,//温5
0x00,0xFE,0x0A,0x8A,0xBE,0xAA,0xAB,0xAA,0xBE,0x8A,0x0A,0x00,0x08,0x07,0x00,0x08,//
0x09,0x0A,0x04,0x04,0x0A,0x09,0x08,0x00,//度6
0x98,0xD4,0xB3,0x88,0x00,0xFE,0x24,0xA8,0xFF,0xA8,0x24,0x00,0x04,0x04,0x02,0x02,//
0x00,0x0F,0x09,0x08,0x0F,0x08,0x09,0x00,//继7
0xFC,0x24,0x24,0x24,0xFF,0x24,0x24,0x24,0xFC,0x00,0x00,0x00,0x03,0x01,0x01,0x01,//
0x07,0x09,0x09,0x09,0x09,0x08,0x0E,0x00,//电8
0x20,0x27,0xA5,0xA5,0x67,0x38,0x67,0xA5,0xAD,0x37,0x20,0x00,0x01,0x0F,0x0A,0x0A,//
0x0E,0x00,0x0E,0x0A,0x0A,0x0F,0x01,0x00,//器0
0x11,0xF2,0x00,0x7A,0x4A,0xCA,0xFF,0xCA,0x4A,0x7A,0x02,0x00,0x08,0x07,0x08,0x0A,//
0x09,0x08,0x0F,0x08,0x09,0x0A,0x08,0x00,//速1
0xF9,0x02,0xF8,0xA9,0xA9,0xF9,0xA9,0xA9,0xF9,0x01,0xFF,0x00,0x0F,0x00,0x00,0x00,//
0x00,0x07,0x00,0x00,0x00,0x08,0x0F,0x00,//闸2
0x00,0x02,0x02,0xC2,0x3E,0x02,0x02,0x02,0x02,0x02,0xFE,0x00,0x08,0x04,0x03,0x00,//
0x00,0x00,0x00,0x08,0x08,0x08,0x07,0x00,//刀3
0x00,0x00,0x00,0x00,0xFF,0x10,0x10,0x10,0x10,0x10,0x00,0x00,0x08,0x08,0x08,0x08,//
0x0F,0x08,0x08,0x08,0x08,0x08,0x08,0x00,//上4
0x02,0x02,0x02,0x02,0xFE,0x02,0x12,0x22,0x42,0x82,0x02,0x00,0x00,0x00,0x00,0x00,//
0x0F,0x00,0x00,0x00,0x00,0x01,0x00,0x00,//下5
0x00,0xFE,0x02,0x42,0x42,0x42,0xFA,0x42,0x42,0x42,0x02,0x00,0x08,0x07,0x08,0x08,//
0x08,0x08,0x0F,0x08,0x09,0x0A,0x08,0x00,//压6
0x22,0x44,0x00,0x24,0xB4,0x2C,0xA5,0x26,0xA4,0x34,0x64,0x00,0x04,0x02,0x08,0x04,//
0x03,0x00,0x0F,0x00,0x07,0x08,0x0E,0x00,//流7
0x11,0x22,0x80,0x3E,0x2A,0xEA,0x2A,0xEA,0x2A,0x3E,0x80,0x00,0x04,0x02,0x08,0x09,//
0x08,0x0F,0x08,0x0F,0x08,0x09,0x08,0x00,//湿8
0x00,0xFE,0x12,0x22,0xC2,0x22,0x1A,0x02,0xFE,0x00,0x00,0x00,0x08,0x07,0x02,0x01,//
0x00,0x01,0x02,0x00,0x03,0x04,0x0F,0x00,//风9
0x40,0x42,0x42,0xFE,0x42,0x42,0x42,0xFE,0x42,0x42,0x40,0x00,0x00,0x08,0x06,0x01,//
0x00,0x00,0x00,0x0F,0x00,0x00,0x00,0x00,//开10
0x40,0x48,0x49,0x4A,0x48,0xF8,0x48,0x4A,0x49,0x48,0x40,0x00,0x08,0x08,0x04,0x02,//
0x01,0x00,0x01,0x02,0x04,0x08,0x08,0x00,//关11
0x04,0x88,0xFF,0x00,0x10,0x10,0xD0,0x3F,0xD0,0x12,0x14,0x00,0x01,0x00,0x0F,0x00,//
0x08,0x06,0x01,0x00,0x01,0x06,0x08,0x00,//状0
0x44,0x44,0x24,0x14,0x0C,0xA7,0x4C,0x14,0x24,0x44,0x44,0x00,0x04,0x03,0x00,0x07,//
0x08,0x08,0x0B,0x08,0x0C,0x01,0x06,0x00,//态1

};


//定义一个结构体用于对应汉字字模在字库中的索引位置查找
typedef struct
{
    SGUI_BYTE   SZUicode[3];	//存放汉字的内码
    SGUI_INT    index;			//汉字在字库数组中的偏移地址（起始地址是95，因为前面有95个ascii半角字符）
}HZ_MAP;

#define HZ_NUM  ((sizeof(GB2312_H12) - 95*12)/24)//自动计算查找表元素个数

const HZ_MAP HZ_info[HZ_NUM] =
{
	  {"列",0, },
    {"表",1, },
    {"组",2, },
    {"件",3, },
    {"基",4, },
    {"础",5, },
    {"绘",6, },
    {"图",7, },
    {"接",8, },
    {"口",9, },
    
    {"文",10,},
    {"本",11,},
    {"输",12,},
    {"入",13,},
    {"框",14,},
    {"实",15,},
    {"时",16,},
    {"曲",17,},
    {"线",18,},
    {"菜",19,},
    
    {"单",20,},
    {"消",21,},
    {"息",22,},
    {"我",23,},
    {"不",24,},
    {"喜",25,},
    {"欢",26,},
    {"这",27,},
    {"个",28,},
    {"里",29,},
    
    {"是",30,},
    {"像",31,},
    {"素",32,},
    {"的",33,},
    {"多",34,},
    {"行",35,},
    {"演",36,},
    {"示",37,},
		
		{"一",38,},
    {"条",39,},
    {"中",40,},
    {"提",41,},
    {"没",42,},
    {"有",43,},
    {"标",44,},
    {"，",45,},
		{"。",46,},
    {"℃",47,},
    {"设",48,},
    {"备",49,},
    {"温",50,},
		
    {"度",51,},
    {"继",52,},
    {"电",53,},
		{"器",54,},
    {"速",55,},
    {"闸",56,},
    {"刀",57,},
    {"上",58,},
    {"下",59,},
    {"压",60,},
		
    {"流",61,},
		{"湿",62,},
    {"风",63,},
    {"开",64,},
    {"关",65,},
		{"状",66,},
    {"态",67,},

}; 


const SGUI_FONT_RES GB2312_FZXS12 =
{
    /*SGUI_INT                      iHalfWidth*/            6,
    /*SGUI_INT                      iFullWidth*/            12,
    /*SGUI_INT                      iHeight*/               12,
	/*SGUI_FN_IF_GET_CHAR_INDEX     fnGetIndex*/            GetCharIndex_GB2312,
	/*SGUI_FN_IF_GET_DATA           fnGetData*/             GB2312_GetFontData,
	/*SGUI_FN_IF_STEP_NEXT          fnStepNext*/            StepNext_GB2312,
	/*SGUI_FN_IF_IS_FULL_WIDTH      fnIsFullWidth*/         GB2312_IsFullWidth
};

//=======================================================================//
//= Function define.										            =//
//=======================================================================//
/*************************************************************************/
/** Function Name:	GetCharIndex_GB2312									**/
/** Purpose:		Get character index in font library.				**/
/** Resources:		None.												**/
/** Params:																**/
/**	@ uiCode[in]:	Character code.										**/
/** Return:			Character index.									**/
/*************************************************************************/
SGUI_INT GetCharIndex_GB2312(SGUI_UINT32 uiCode)
{
    SGUI_INT                    iIndex;
    SGUI_UINT32                 i;
    SGUI_UINT32                 uiCodeMap;
    iIndex =                    SGUI_INVALID_INDEX;

    /* for ASCII characters. */
    if((uiCode > 0x19) && (uiCode < 0x7F))
    {
        iIndex = uiCode - (0x20);
    }
    else
    {
        //例如：A1A1 算出index是95，index的跨度是半角字符所占字节个数12个字符
        for(i=0;i<HZ_NUM;i++)
        {
            uiCodeMap =  HZ_info[i].SZUicode[1] + (HZ_info[i].SZUicode[0]<<8);
            if(uiCodeMap == uiCode)
            {
                iIndex = HZ_info[i].index*2 + 95;
                break;
            }
        }
        
        if(i == HZ_NUM)iIndex = 95;
    }
    return iIndex;
}



/*************************************************************************/
/** Function Name:	StepNext_GB2312									    **/
/** Purpose:		Read current character code order by input pointer  **/
/**                 and step to next character start pointer.			**/
/** Resources:		None.												**/
/** Params:																**/
/**	@ cszSrc[in]:	Current char pointer.								**/
/**	@ puiCode[in]:	Character code.										**/
/** Return:			Next character start pointer.   					**/
/*************************************************************************/
SGUI_CSZSTR StepNext_GB2312(SGUI_CSZSTR cszSrc, SGUI_UINT32* puiCode)
{
    /*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
	const SGUI_CHAR*            pcNextChar;
	SGUI_UINT32					uiCode;

	/*----------------------------------*/
	/* Initialize						*/
	/*----------------------------------*/
	pcNextChar =                cszSrc;
	uiCode =					0;

	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	if(NULL != pcNextChar)
    {
    	do
		{
			uiCode = (SGUI_BYTE)(*pcNextChar++);
			if(uiCode < 0x7F)
			{
				break;
			}
			uiCode = uiCode<<8;
			uiCode |= (SGUI_BYTE)(*pcNextChar++);
		}while(0);
    }
	*puiCode = uiCode;

    return pcNextChar;
}

/*************************************************************************/
/** Function Name:	GB2312_GetFontData								    **/
/** Purpose:		Read character data form font data.         		**/
/** Resources:		None.												**/
/** Params:																**/
/**	@ sStartAddr[in]: Read start address in memory.						**/
/**	@ pDataBuffer[in]: Character data dump buffer pointer.				**/
/**	@ sReadSize[in]: Size of data will be read, always mean the buffer  **/
/**                 size.                               				**/
/** Return:			Data in process was be read.        				**/
/*************************************************************************/
SGUI_SIZE GB2312_GetFontData(SGUI_SIZE sStartAddr, SGUI_BYTE* pDataBuffer, SGUI_SIZE sReadSize)
{
	/*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
	SGUI_SIZE					sReadCount;
	const SGUI_BYTE*			pSrc = GB2312_H12+sStartAddr;
	SGUI_BYTE*					pDest = pDataBuffer;

	/*----------------------------------*/
	/* Initialize						*/
	/*----------------------------------*/
	pSrc =						GB2312_H12+sStartAddr;
	pDest =						pDataBuffer;

	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	if(NULL != pDataBuffer)
	{
		for(sReadCount=0; sReadCount<sReadSize; sReadCount++)
		{
			*pDest++ = *pSrc++;
		}
	}
	return sReadCount;
}

/*************************************************************************/
/** Function Name:	GB2312_GetFontData								    **/
/** Purpose:		Read character data form font data.         		**/
/** Resources:		None.												**/
/** Params:																**/
/**	@ sStartAddr[in]: Read start address in memory.						**/
/**	@ pDataBuffer[in]: Character data dump buffer pointer.				**/
/**	@ sReadSize[in]: Size of data will be read, always mean the buffer  **/
/**                 size.                               				**/
/** Return:			Data in process was be read.        				**/
/*************************************************************************/
SGUI_BOOL GB2312_IsFullWidth(SGUI_UINT32 uiCode)
{
    /*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
	SGUI_BOOL					bReturn;

	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
    if(uiCode < 0x7F)
	{
		bReturn = SGUI_FALSE;
	}
	else
	{
		bReturn = SGUI_TRUE;
	}

	return bReturn;
}

