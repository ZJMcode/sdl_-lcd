//=======================================================================//
//= Include files.													    =//
//=======================================================================//
//#include "DemoProc.h"
#include "SGUI_Port.h"
#include "HMI_Core.h"
#include "SGUI_List.h"
#include "SGUI_FontResource.h"
#include "Resource.h"
//#include "Key.h"
//=======================================================================//
//= User Macro definition.											    =//
//=======================================================================//
//#define					NOTICE_TEXT_BUFFER_SIZE				(64)

//=======================================================================//
//= Static function declaration.									    =//
//=======================================================================//
static HMI_ENGINE_RESULT	HMI_DemoList_Initialize(SGUI_SCR_DEV* pstDeviceIF);
static HMI_ENGINE_RESULT	HMI_DemoList_Prepare(SGUI_SCR_DEV* pstDeviceIF, const void* pstParameters);
static HMI_ENGINE_RESULT	HMI_DemoList_RefreshScreen(SGUI_SCR_DEV* pstDeviceIF, const void* pstParameters);
static HMI_ENGINE_RESULT	HMI_DemoList_ProcessEvent(SGUI_SCR_DEV* pstDeviceIF, const HMI_EVENT_BASE* pstEvent, SGUI_INT* piActionID);
static HMI_ENGINE_RESULT	HMI_DemoList_PostProcess(SGUI_SCR_DEV* pstDeviceIF, HMI_ENGINE_RESULT eProcResult, SGUI_INT iActionID);

//=======================================================================//
//= Static variable declaration.									    =//
//=======================================================================//
static SGUI_ITEMS_ITEM		s_arrstListItems[] =		{
															{SCR1_LIST_ITEM1, NULL,NULL,NULL},
															{SCR1_LIST_ITEM2, NULL,NULL,NULL},
															{SCR1_LIST_ITEM3, NULL,NULL,NULL},
															{SCR1_LIST_ITEM4, NULL,NULL,NULL},
															{SCR1_LIST_ITEM5, NULL,NULL,NULL},
															{SCR1_LIST_ITEM6, NULL,NULL,NULL},
															{SCR1_LIST_ITEM7, NULL,NULL,NULL},
															{SCR1_LIST_ITEM8, NULL,NULL,NULL},
															{SCR1_LIST_ITEM9, NULL,NULL,NULL}

														};
static SGUI_LIST		s_stDemoListObject = 		{0x00};

//=======================================================================//
//= Global variable declaration.									    =//
//=======================================================================//
HMI_SCREEN_ACTION		s_stDemoListActions =			{	HMI_DemoList_Initialize,
															HMI_DemoList_Prepare,
															HMI_DemoList_RefreshScreen,
															HMI_DemoList_ProcessEvent,
															HMI_DemoList_PostProcess
														};
HMI_SCREEN_OBJECT       g_stHMIDemo_List =				{	HMI_SCREEN_ID_DEMO_LIST,
															&s_stDemoListActions,
															EVENT_TYPE_ACTION,
															EVENT_ID_KEY_PRESS,
														};

//=======================================================================//
//= Function define.										            =//
//=======================================================================//
HMI_ENGINE_RESULT HMI_DemoList_Initialize(SGUI_SCR_DEV* pstDeviceIF)
{
	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
		//Initialize Event
		g_stHMIDemo_List.i_EventID = EVENT_ID_KEY_PRESS;
		g_stHMIDemo_List.i_EventType = EVENT_TYPE_ACTION;
    // Initialize list data.
    SGUI_SystemIF_MemorySet(&s_stDemoListObject, 0x00, sizeof(SGUI_LIST));
    // Title and font size must set before initialize list object.
    s_stDemoListObject.stLayout.iX = 0;
    s_stDemoListObject.stLayout.iY = 0;
    s_stDemoListObject.stLayout.iWidth = LCD_Point_W;
    s_stDemoListObject.stLayout.iHeight = LCD_Point_H;
    s_stDemoListObject.szTitle = SCR1_TITLE;
     //Initialize list object.
		SGUI_List_Initialize(&s_stDemoListObject, &s_stDemoListObject.stLayout,&GB2312_FZXS12, s_stDemoListObject.szTitle,s_arrstListItems, sizeof(s_arrstListItems)/sizeof(SGUI_ITEMS_ITEM));
	return HMI_RET_NORMAL;
}

HMI_ENGINE_RESULT HMI_DemoList_Prepare (SGUI_SCR_DEV* pstDeviceIF, const void* pstParameters)
{
	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	SGUI_List_Repaint(pstDeviceIF, &s_stDemoListObject);
	return HMI_RET_NORMAL;
}

HMI_ENGINE_RESULT HMI_DemoList_RefreshScreen(SGUI_SCR_DEV* pstDeviceIF, const void* pstParameters)
{
	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	SGUI_List_Repaint(pstDeviceIF, &s_stDemoListObject);
	return HMI_RET_NORMAL;
}

HMI_ENGINE_RESULT HMI_DemoList_ProcessEvent(SGUI_SCR_DEV* pstDeviceIF, const HMI_EVENT_BASE* pstEvent, SGUI_INT* piActionID)
{
	/*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
	HMI_ENGINE_RESULT           eProcessResult;
	SGUI_UINT16					uiKeyCode;
	SGUI_UINT16					uiKeyValue;
	KEY_PRESS_EVENT*			pstKeyEvent;
	SGUI_INT					iProcessAction;

	/*----------------------------------*/
	/* Initialize						*/
	/*----------------------------------*/
	eProcessResult =			HMI_RET_NORMAL;
	pstKeyEvent =				(KEY_PRESS_EVENT*)pstEvent;
	iProcessAction =			HMI_DEMO_PROC_NO_ACT;

	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	if(pstEvent->iType == EVENT_TYPE_ACTION)
	{
		// Check event is valid.
		if(SGUI_FALSE == HMI_EVENT_SIZE_CHK(*pstKeyEvent, KEY_PRESS_EVENT))
		{
			// Event data is invalid.
			eProcessResult = HMI_RET_INVALID_DATA;
		}
		else if(EVENT_ID_KEY_PRESS == pstEvent->iID)
		{
			uiKeyCode = pstKeyEvent->Data.uiKeyValue;
			uiKeyValue = KEY_CODE_VALUE(uiKeyCode);
			pstKeyEvent->Data.uiKeyValue = KEY_VALUE_NONE;
			switch(uiKeyValue)
			{
				case KEY_VALUE_ENTER:
				{

					iProcessAction = HMI_DEMO_PROC_CONFIRM;
					break;
				}
				case KEY_VALUE_ESC:
				{
					iProcessAction = HMI_DEMO_PROC_CANCEL;
					break;
				}
				case KEY_VALUE_UP:
				{
					if(s_stDemoListObject.stItems.stSelection.iIndex > 0)
					{
						s_stDemoListObject.stItems.stSelection.iIndex -= 1;
					}
					s_stDemoListObject.stItems.stSelection.pstItem = &s_arrstListItems[s_stDemoListObject.stItems.stSelection.iIndex];
					SGUI_List_Repaint(pstDeviceIF, &s_stDemoListObject);
					break;
				}
				case KEY_VALUE_DOWN:
				{
					if(s_stDemoListObject.stItems.stSelection.iIndex < s_stDemoListObject.stItems.iCount-1)
					{
						s_stDemoListObject.stItems.stSelection.iIndex += 1;
					}
					s_stDemoListObject.stItems.stSelection.pstItem = &s_arrstListItems[s_stDemoListObject.stItems.stSelection.iIndex];
					SGUI_List_Repaint(pstDeviceIF, &s_stDemoListObject);
					break;
				}
				default:
				{
					break;
				}
			}
		}
	}
	if(NULL != piActionID)
	{
		*piActionID = iProcessAction;
	}

	return eProcessResult;
}

HMI_ENGINE_RESULT HMI_DemoList_PostProcess(SGUI_SCR_DEV* pstDeviceIF, HMI_ENGINE_RESULT eProcResult, SGUI_INT iActionID)
{
	if(HMI_DEMO_PROC_CONFIRM == iActionID)
	{
      switch(s_stDemoListObject.stItems.stSelection.iIndex)
      {
      case 0:
			{
				HMI_SwitchScreen(HMI_SCREEN_ID_DEMO_BASIC_PAINT, NULL);
				break;
			}
			case 1:
			{
				HMI_SwitchScreen(HMI_SCREEN_ID_DEMO_PAINT_TEXT, NULL);
				break;
			}
			case 2:
			{
				HMI_SwitchScreen(HMI_SCREEN_ID_DEMO_VARIABLE_BOX, NULL);
				break;
			}
			case 3:
			{
				HMI_SwitchScreen(HMI_SCREEN_ID_DEMO_REAL_TIME_GRAPH, NULL);
				break;
			}
			case 4:
			{
				HMI_SwitchScreen(HMI_SCREEN_ID_DEMO_MENU, NULL);
				break;
			}
			case 5:
			{
				HMI_SwitchScreen(HMI_SCREEN_ID_DEMO_TEXT_NOTICE, SCR7_NOTICE_TEXT);
				break;
			}
			case 6:
			{
				HMI_SwitchScreen(HMI_SCREEN_ID_VARIABLE_LIST, NULL);
				break;
			}
			case 7:
			{
				HMI_SwitchScreen(HMI_SCREEN_ID_INPUT_INT_BOX, NULL);
				break;
			}
			case 8:
			{
				HMI_SwitchScreen(HMI_SCREEN_ID_VARIABLE_INPUT_INIT_LIST, NULL);
				break;
			}
			default:
			{

				/* do nothing. */
			}
        }
	}
	else if(HMI_DEMO_PROC_CANCEL == iActionID)
	{
		HMI_GoBack(NULL);
	}

	return HMI_RET_NORMAL;
}


