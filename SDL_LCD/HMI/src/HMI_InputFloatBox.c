//=======================================================================//
//= Include files.													    =//
//=======================================================================//
//#include "DemoProc.h"
#include "SGUI_Port.h"
#include "HMI_Core.h"
#include "Resource.h"
#include "SGUI_Notice.h"
#include "SGUI_VariableBox.h"
#include "SGUI_FontResource.h"
#include "SGUI_IconResource.h"
#include "MVC_Control.h"
//=======================================================================//
//= User Macro definition.											    =//
//=======================================================================//
#define						TEXT_INPUT_FLOAT_LENGTH				(11)

#define						INPUT_FLOAT_BOX_WIDTH					(100)
#define						INPUT_FLOAT_NUMBER_BOX_HEIGHT			(8)
#define						INPUT_FLOAT_TEXT_BOX_HEIGHT			(12)
#define						INPUT_FLOAT_BOX_POSX					(10)
#define						INPUT_FLOAT_BOX_NUMBER_POSY			(19)
#define						INPUT_FLOAT_BOX_TEXT_POSY				(38)
//=======================================================================//
//= Static function declaration.									    =//
//=======================================================================//
static HMI_ENGINE_RESULT    HMI_InputFloatBox_Initialize(SGUI_SCR_DEV* pstDeviceIF);
static HMI_ENGINE_RESULT	HMI_InputFloatBox_Prepare(SGUI_SCR_DEV* pstDeviceIF, const void* pstParameters);
static HMI_ENGINE_RESULT	HMI_InputFloatBox_RefreshScreen(SGUI_SCR_DEV* pstDeviceIF, const void* pstParameters);
static HMI_ENGINE_RESULT    HMI_InputFloatBox_ProcessEvent(SGUI_SCR_DEV* pstDeviceIF, const HMI_EVENT_BASE* pstEvent, SGUI_INT* piActionID);
static HMI_ENGINE_RESULT	HMI_InputFloatBox_PostProcess(SGUI_SCR_DEV* pstDeviceIF, HMI_ENGINE_RESULT eProcResult, SGUI_INT iActionID);
static void				    HMI_InputFloatBox_DrawFrame(SGUI_SCR_DEV* pstDeviceIF, SGUI_SZSTR szTitle);

//=======================================================================//
//= Static variable declaration.									    =//
//=======================================================================//
static SGUI_NUM_VARBOX_STRUCT	s_stFloatNumberVariableBox ;
const	SGUI_NUM_VARBOX_PARAM	s_stFloatNumberVariableBoxParam =
	{
		{INPUT_FLOAT_BOX_POSX+2, INPUT_FLOAT_BOX_NUMBER_POSY+2, INPUT_FLOAT_BOX_WIDTH, INPUT_FLOAT_NUMBER_BOX_HEIGHT},
		-5000,
		5000,
		&SGUI_DEFAULT_FONT_8,
		SGUI_CENTER
	};

static SGUI_CHAR				s_szFloatTextVariableBuffer[TEXT_INPUT_FLOAT_LENGTH+1] = {"0123456789."};

static SGUI_TEXT_VARBOX_STRUCT	s_stFloatTextVariableBox;
const SGUI_TEXT_VARBOX_PARAM s_stFloatTextVariableBoxParam = {
		{INPUT_FLOAT_BOX_POSX+2, INPUT_FLOAT_BOX_TEXT_POSY+2, INPUT_FLOAT_BOX_WIDTH, INPUT_FLOAT_TEXT_BOX_HEIGHT},
		&SGUI_DEFAULT_FONT_12,
		TEXT_INPUT_FLOAT_LENGTH,
};

static SGUI_CHAR				s_szDefaultFrameTitle[] =	SCR10_TITLE;
static SGUI_SZSTR				s_szFrameTitle =			s_szDefaultFrameTitle;
static SGUI_INT					s_uiFocusedFlag;
static SGUI_CSZSTR				s_szHelpNoticeText =		SCR4_HELP_NOTICE;

static SGUI_UINT32 			u32_VariableNumber = 0;//输入变量
static SGUI_UINT8				u8_VariableNumberCnt =		0;//输入变量计数
static VariableInput*   st_ptrVariableInput ;//MVC输入数组指针
static SGUI_BOOL        b_DecimalEnter = false;
static SGUI_INT 				iDecimalPlaces = 0xFF;//小数点位置

HMI_SCREEN_ACTION				s_stInputFloatBoxActions = {
																HMI_InputFloatBox_Initialize,
																HMI_InputFloatBox_Prepare,
																HMI_InputFloatBox_RefreshScreen,
																HMI_InputFloatBox_ProcessEvent,
																HMI_InputFloatBox_PostProcess,
															};

//=======================================================================//
//= Global variable declaration.									    =//
//=======================================================================//
HMI_SCREEN_OBJECT       		g_stHMI_InputFloatBox =	{	HMI_SCREEN_ID_VARIABLE_INPUT_FLOAT,
																&s_stInputFloatBoxActions
															};

//=======================================================================//
//= Function define.										            =//
//=======================================================================//
HMI_ENGINE_RESULT HMI_InputFloatBox_Initialize(SGUI_SCR_DEV* pstDeviceIF)
{
	//Initialize Event
	g_stHMI_InputFloatBox.i_EventID = EVENT_ID_KEY_PRESS;
	g_stHMI_InputFloatBox.i_EventType = EVENT_TYPE_ACTION;

    SGUI_NumberVariableBox_Initialize(&s_stFloatNumberVariableBox, &s_stFloatNumberVariableBoxParam);//初始化NUM Param
	SGUI_TextVariableBox_Initialize(&s_stFloatTextVariableBox,&s_stFloatTextVariableBoxParam, s_szFloatTextVariableBuffer);
	s_uiFocusedFlag = 0;
	return HMI_RET_NORMAL;
}

HMI_ENGINE_RESULT HMI_InputFloatBox_Prepare(SGUI_SCR_DEV* pstDeviceIF, const void* pstParameters)
{
	/*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
//	SGUI_NOTICT_BOX           	stNoticeBox;

	/*----------------------------------*/
	/* Initialize						*/
	/*----------------------------------*/
//	stNoticeBox.pstIcon = &SGUI_RES_ICON_INFORMATION_16;
//	stNoticeBox.cszNoticeText = s_szHelpNoticeText;
	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	u32_VariableNumber = 0;
	s_uiFocusedFlag = 0;
	u8_VariableNumberCnt = 0;
	iDecimalPlaces = 0;
	b_DecimalEnter = false;

	st_ptrVariableInput = (VariableInput*)pstParameters;
	// Draw frame
	s_szFrameTitle = s_szDefaultFrameTitle;
	HMI_InputFloatBox_DrawFrame(pstDeviceIF, (SGUI_SZSTR)s_szFrameTitle);

	// Draw number box
	s_stFloatNumberVariableBox.stData.iValue = u32_VariableNumber;
	SGUI_Basic_DrawRectangle(pstDeviceIF, INPUT_FLOAT_BOX_POSX, INPUT_FLOAT_BOX_NUMBER_POSY, INPUT_FLOAT_BOX_WIDTH+4, INPUT_FLOAT_NUMBER_BOX_HEIGHT+4, SGUI_COLOR_FRGCLR, SGUI_COLOR_BKGCLR);
	SGUI_FloatVariableBox_Repaint(pstDeviceIF, &s_stFloatNumberVariableBox, iDecimalPlaces,SGUI_DRAW_REVERSE);
	// Draw text box
	s_stFloatTextVariableBox.stData.iFocusIndex = 0;
	SGUI_Basic_DrawRectangle(pstDeviceIF, INPUT_FLOAT_BOX_POSX, INPUT_FLOAT_BOX_TEXT_POSY, INPUT_FLOAT_BOX_WIDTH+4, INPUT_FLOAT_TEXT_BOX_HEIGHT+4, SGUI_COLOR_FRGCLR, SGUI_COLOR_BKGCLR);
	SGUI_TextVariableBox_Repaint(pstDeviceIF, &s_stFloatTextVariableBox, '\0', (0 == s_uiFocusedFlag)?SGUI_DRAW_NORMAL:SGUI_DRAW_REVERSE);

	return HMI_RET_NORMAL;
}

HMI_ENGINE_RESULT HMI_InputFloatBox_RefreshScreen(SGUI_SCR_DEV* pstDeviceIF, const void* pstParameters)
{
     /*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	// Draw frame
    HMI_InputFloatBox_DrawFrame(pstDeviceIF, (SGUI_SZSTR)s_szFrameTitle);
    // Draw number box
	SGUI_Basic_DrawRectangle(pstDeviceIF, INPUT_FLOAT_BOX_POSX, INPUT_FLOAT_BOX_NUMBER_POSY, INPUT_FLOAT_BOX_WIDTH+4, INPUT_FLOAT_NUMBER_BOX_HEIGHT+4, SGUI_COLOR_FRGCLR, SGUI_COLOR_BKGCLR);
    SGUI_FloatVariableBox_Repaint(pstDeviceIF, &s_stFloatNumberVariableBox, iDecimalPlaces,(0 == s_uiFocusedFlag)?SGUI_DRAW_REVERSE:SGUI_DRAW_NORMAL );
    // Draw text box
    SGUI_Basic_DrawRectangle(pstDeviceIF, INPUT_FLOAT_BOX_POSX, INPUT_FLOAT_BOX_NUMBER_POSY, INPUT_FLOAT_BOX_WIDTH+4, INPUT_FLOAT_TEXT_BOX_HEIGHT+4, SGUI_COLOR_FRGCLR, SGUI_COLOR_BKGCLR);
    SGUI_TextVariableBox_Repaint(pstDeviceIF, &s_stFloatTextVariableBox, '\0', (0 == s_uiFocusedFlag)?SGUI_DRAW_NORMAL:SGUI_DRAW_REVERSE);
		//SGUI_TextVariableBox_Paint(pstDeviceIF, &s_stTextVariableBox, (0 == s_uiFocusedFlag)?SGUI_DRAW_NORMAL:SGUI_DRAW_REVERSE);

	return HMI_RET_NORMAL;
}

HMI_ENGINE_RESULT HMI_InputFloatBox_ProcessEvent(SGUI_SCR_DEV* pstDeviceIF, const HMI_EVENT_BASE* pstEvent, SGUI_INT* piActionID)
{
	/*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
	HMI_ENGINE_RESULT           eProcessResult;
	SGUI_UINT16					uiKeyValue;
	KEY_PRESS_EVENT*			pstKeyEvent;
	SGUI_INT					iProcessAction;

	/*----------------------------------*/
	/* Initialize						*/
	/*----------------------------------*/
	eProcessResult =			HMI_RET_NORMAL;
	iProcessAction =			HMI_DEMO_PROC_NO_ACT;

	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
			if(EVENT_TYPE_ACTION == pstEvent->iType && EVENT_ID_KEY_PRESS == pstEvent->iID)
			{
				pstKeyEvent = (KEY_PRESS_EVENT*)pstEvent;
				uiKeyValue = KEY_CODE_VALUE(pstKeyEvent->Data.uiKeyValue);

			switch(uiKeyValue)
			{
				case KEY_VALUE_TAB:
				{
					s_uiFocusedFlag = ((s_uiFocusedFlag+1)%2);
					if(0 == s_uiFocusedFlag)
					{
						SGUI_FloatVariableBox_Repaint(pstDeviceIF, &s_stFloatNumberVariableBox, iDecimalPlaces,SGUI_DRAW_REVERSE);
						SGUI_TextVariableBox_Repaint(pstDeviceIF, &s_stFloatTextVariableBox,'\0', SGUI_DRAW_NORMAL);
					}
					else
					{
						SGUI_FloatVariableBox_Repaint(pstDeviceIF, &s_stFloatNumberVariableBox, iDecimalPlaces,SGUI_DRAW_NORMAL);
						SGUI_TextVariableBox_Repaint(pstDeviceIF, &s_stFloatTextVariableBox,'\0', SGUI_DRAW_REVERSE);
					}
					break;
				}
				case KEY_VALUE_ESC:
				{
					iProcessAction = HMI_DEMO_PROC_CANCEL;
					break;
				}
				case KEY_VALUE_DOWN:
				{
					if(1 == s_uiFocusedFlag)
					{
						if(s_stFloatTextVariableBox.stData.iFocusIndex > 0)
						{
							s_stFloatTextVariableBox.stData.iFocusIndex--;
							//SGUI_TextVariableBox_ChangeCharacter(pstDeviceIF, &s_stTextVariableBox, SGUI_DRAW_REVERSE, SGUI_TEXT_ASCII, SGUI_TXT_VARBOX_OPT_NONE);
							SGUI_TextVariableBox_Repaint(pstDeviceIF, &s_stFloatTextVariableBox, '\0', SGUI_DRAW_REVERSE);
						}
					}
					break;
				}
				case KEY_VALUE_UP:
				{
					if(1 == s_uiFocusedFlag)
					{
						if(s_stFloatTextVariableBox.stData.iFocusIndex < (s_stFloatTextVariableBox.stParam.iTextLength-1))
						{
							s_stFloatTextVariableBox.stData.iFocusIndex++;
							SGUI_TextVariableBox_Repaint(pstDeviceIF, &s_stFloatTextVariableBox, '\0', SGUI_DRAW_REVERSE);
						}
					}
					break;
				}
				case KEY_VALUE_ENTER:
				{
					if(1 == s_uiFocusedFlag)
					{
						// Draw number box
						if(u8_VariableNumberCnt||s_stFloatTextVariableBox.stData.iFocusIndex)//过滤掉刚开始一直输入0的情况
						{
							u8_VariableNumberCnt++;//把输入值限定在9位数，否则有数据溢出的风险
						}
						if(u8_VariableNumberCnt<10)
						{
							if(s_stFloatTextVariableBox.stData.iFocusIndex<10)//判断是否选择的是小数点
							{
								u32_VariableNumber = u32_VariableNumber*10+s_stFloatTextVariableBox.stData.iFocusIndex;
								if(b_DecimalEnter)
								{
									iDecimalPlaces++;
								}
							}
							else
							{
								b_DecimalEnter = true;
							}
						}
						else
						{
							u8_VariableNumberCnt = 10;
						}
						 // Draw number box
						s_stFloatNumberVariableBox.stData.iValue = u32_VariableNumber;
						SGUI_Basic_DrawRectangle(pstDeviceIF, INPUT_FLOAT_BOX_POSX, INPUT_FLOAT_BOX_NUMBER_POSY, INPUT_FLOAT_BOX_WIDTH+4, INPUT_FLOAT_NUMBER_BOX_HEIGHT+4, SGUI_COLOR_FRGCLR, SGUI_COLOR_BKGCLR);
						SGUI_FloatVariableBox_Repaint(pstDeviceIF, &s_stFloatNumberVariableBox, iDecimalPlaces,SGUI_DRAW_NORMAL);
					}
					else
					{
						iProcessAction = HMI_DEMO_PROC_CONFIRM;
					}
					break;
				}
				default:
				{
					/* No process. */
					break;
				}
			}
    }

  if(NULL != piActionID)
	{
		*piActionID = iProcessAction;
	}

	return eProcessResult;
}

HMI_ENGINE_RESULT HMI_InputFloatBox_PostProcess(SGUI_SCR_DEV* pstDeviceIF, HMI_ENGINE_RESULT eProcResult, SGUI_INT iActionID)
{
	if(HMI_PROCESS_SUCCESSFUL(eProcResult))
	{
		if(HMI_DEMO_PROC_CANCEL == iActionID)
		{
			HMI_GoBack(NULL);
		}
		if(HMI_DEMO_PROC_CONFIRM == iActionID)
		{
			if(NULL != st_ptrVariableInput)
			{
				float f_VariableFloatNumber = u32_VariableNumber;
				for(char i = 0;i < iDecimalPlaces;i++)
				{
					f_VariableFloatNumber = f_VariableFloatNumber*0.1;
				}
				st_ptrVariableInput->VariableInput.fValue = f_VariableFloatNumber;
			}
			HMI_GoBack(NULL);
		}

	}

	return HMI_RET_NORMAL;
}

void HMI_InputFloatBox_DrawFrame(SGUI_SCR_DEV* pstDeviceIF, SGUI_SZSTR szTitle)
{
	/*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
	SGUI_RECT				stTextDisplayArea;
	SGUI_POINT				stInnerPos;

	/*----------------------------------*/
	/* Initialize						*/
	/*----------------------------------*/
	stTextDisplayArea.iX =	4;
	stTextDisplayArea.iY =	4;
	stTextDisplayArea.iHeight = 12;
	stInnerPos.iX =			0;
	stInnerPos.iY =			0;

	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	if(NULL != pstDeviceIF)
	{
		stTextDisplayArea.iWidth = pstDeviceIF->stSize.iWidth-8;
		SGUI_Basic_DrawRectangle(pstDeviceIF, 0, 0, SGUI_RECT_WIDTH(pstDeviceIF->stSize), SGUI_RECT_HEIGHT(pstDeviceIF->stSize), SGUI_COLOR_FRGCLR, SGUI_COLOR_BKGCLR);
		SGUI_Basic_DrawRectangle(pstDeviceIF, 2, 2, SGUI_RECT_WIDTH(pstDeviceIF->stSize)-4, SGUI_RECT_HEIGHT(pstDeviceIF->stSize)-4, SGUI_COLOR_FRGCLR, SGUI_COLOR_TRANS);
		SGUI_Basic_DrawLine(pstDeviceIF, 3, 17, 124, 17, SGUI_COLOR_FRGCLR);
		SGUI_Text_DrawText(pstDeviceIF, szTitle, &GB2312_FZXS12, &stTextDisplayArea, &stInnerPos, SGUI_DRAW_NORMAL);
	}
}
