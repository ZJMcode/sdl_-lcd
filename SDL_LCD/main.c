
#include "LCD_def.h"
#include "SGUI_Port.h"
#include "HMI_Core.h"
#include "Key.h"

int main(int argc,char* argv[])//完整的mian函数定义
{
    SDL_Init(SDL_INIT_VIDEO);//初始化SDL库
    lcd_beginSDL();
    lcd_CreatWindow_surface_texture();

    SGUI_Init();
    //SGUI_Test();
    InitHMIEngineObj();

    while(quit)
    {
        Key_HandleEvent();
        HMI_Work();

    }


    lcd_Destroy();//退出SDL

    return 0;
}
