感谢SGUI作者Polarix、XuYulin等的开源，开源地址https://gitee.com/Polarix/simplegui?_from=gitee_search

本项目是为了配套SimpleGUI在PC端开发而创建的，Polarix大佬也公布了他的一个LCD的模拟器开发环境，但是我搭建失败了，哈哈，所以我决定自己写一个。之前大学有用过SDL，用SDL模拟单色屏开发环境，个人感觉还是比较容易的，当然写一个简单彩色屏的模拟开发环境也不难。言归正传：

一、环境搭建：

CodeBlocks版本：   20.03

**安装好CodeBlocks 20.03 ，使用者下载项目即用，不需要额外操作！！**

环境搭建参考：

[CodeBlocks下载安装与SDL下载使用配置教程_张江一哥的博客-CSDN博客_codeblock sdl](https://blog.csdn.net/Foxproll/article/details/112249355)

工程中带了SDL2的包，工程下的SDL2文件夹就是SDL2的包。参考张江一哥的配置，在配置文件关联的时候注意一下：

![image-20220912135335823](image-20220912135335823.png)

![image-20220912135501358](image-20220912135501358.png)



二、参数介绍：

实现LCD模拟环境的文件就三个，lyc文件夹下的三个文件：

LCD_def.h、LCD_function.h、LCD_function.c

LCD_function中实现了SDL模拟单色屏的函数，创建窗口、更新显示等功能。

LCD_def.h中的定义了LCD的参数：

```c
#define LCD_Point_Pixel 4//lcd上的点的大小
#define LCD_Point_Pixel_Interval 1//lcd上的点之间像素大小
#define LCD_Point_W 128 //lcd上的宽度上的点数
#define LCD_Point_H 64 //lcd上的高度上的点数

#define USE_Slow_Time 0
#define Slow_Time_ms 1

#define Single_Row_Method 0
#define Single_Line_Method 1
#define Row_Line_Method 0
#define Line_Row_Method 0

#define Yin_Yang_Code 0 //1阳码 0阴码
#define Order_ReverseOrder 1//1顺向 0 逆向 从第一列开始向下 顺序取模就是取8个点作为一个字节，从低到高；逆序就是从高到低

#define LCD_Point_Background_Color 0, 0, 0, 180 //R,G,B,透明度
#define LCD_Point_Enable_Color 0, 255, 255, 255 //R,G,B,透明度
#define LCD_Point_Disenable_Color 0, 0, 255, 180 //R,G,B,透明度

```

LCD_Point_Pixel 定义了模拟LCD屏上的单个像素点占几个像素宽度。

LCD_Point_Pixel_Interval 定义LCD单个像素点之间的间隔，单位像素。

LCD_Point_W 模拟LCD上宽度有多少个点；LCD_Point_H高度上有多少个点。

这四个参数作用见下图：



![image-20220911215352772](image-20220911215352772.png)

![image-20220911220655501](image-20220911220655501.png)

下图lcd配置：宽度上256个点，高度上128个点，每个点像素宽度为2，点与点之间宽度为0

![image-20220911235818126](image-20220911235818126.png)





USE_Slow_Time 是否使用延迟刷新，Slow_Time_ms刷新点与点之间的间隔，单位毫秒。该功能是为了让初学者更好的理解不同刷新方式的具体是怎么呈现画面的，见下图：

逐行扫描，逆向



![tutieshi_640x320_13s](tutieshi_640x320_13s.gif)

行列扫描，逆向

![tutieshi_640x320_15s](tutieshi_640x320_15s.gif)

不同的LCD扫描方式和点亮逻辑不一样，具体网上介绍很多，我就不在这赘述了。

主要区别为：

1、阴码、阳码

#define Yin_Yang_Code 0 //1阳码 0阴码

2、逐列扫描、逐行扫描、行列扫描、列行扫描 0代表失能，1代表使能

#define Single_Row_Method 0
#define Single_Line_Method 1
#define Row_Line_Method 0
#define Line_Row_Method 0

3、顺向、逆向(数据的大小端)

#define Order_ReverseOrder 1//1顺向 0 逆向 

![微信图片_20220911210931](微信图片_20220911210931.jpg)

4、颜色

大家根据喜好改变配色：

#define LCD_Point_Background_Color 0, 0, 0, 180 //R,G,B,透明度 lcd背景色
#define LCD_Point_Enable_Color 0, 255, 255, 255 //R,G,B,透明度 亮的颜色
#define LCD_Point_Disenable_Color 0, 0, 255, 180 //R,G,B,透明度 灭的颜色

![image-20220911233813335](image-20220911233813335.png)

![image-2022091123415209](image-2022091123415209.png)

三、按键

按键事件用的是SDL的按键检测，移植的时候这部分需要根据自己的项目在单片机上实现。

思路都是一样的：

不同按键按下、就改变全局变量uiKeyValue的值，然后HMI会进行处理。

具体看HMI文件夹下的Key.c和Key.h

四、接口介绍

1、我们在LCD_function.c中创建了char lcd_points [LCD_Point_W       LCD_Point_H] 数组，该数组每个元素代表一个像素点，在阴码模式下，0代表灭，1代表亮；阳码相反。lcd_points代表的是我们模拟LCD驱动芯片中的显存，比如我们常用的128X64 的驱动芯片SSD1306，我们通过SPI往驱动芯片的显示缓冲区128 x 64位SRAM写数据。

2、SGUI_Port.c中定义了SGUI的显存 char OLED_Buffer[Buffer_size]。

3、lcd_points代表的是我们模拟LCD中固有的显存，OLED_Buffer是单片机上我们开辟的栈。我们实现就是根据OLED_Buffer的每个位的状态，改变对应lcd_points中的元素状态。因此我们实现SGUI_Port.c中 写点、读点、刷新、清空、填充块，都是根据这个思路编写。

大家往单片机移植的时候需要在单片机上实现这些接口，可以参考SGUI作者的例程。

4、GUI文件夹下的SGUI_Port.c是SGUI的接口，在SGUI_Port.c，我们根据SGUI作者的要求，实现 写点、读点、刷新、清空、填充块等函数。SGUI_Test()函数是写的测试GUI功能。

5、HMI文件夹下的HMI_Core.c是HMI的接口，在HMI添加了设备状态、输入、输入态界面。

项目需完善的点：

1、因为我手上只有行列扫描、阴码、逆向的LCD，目前只支持该扫描方式，文字、图片都是按照该格式生成的。其他扫描方式的LCD移植到单片机上，麻烦一些，需要更换相应的文字格式。----后续会完善

